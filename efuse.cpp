/*
 * Copyright (C) 2011 Amlogic Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <malloc.h>
#include <asm/byteorder.h>
#include <linux/types.h>

#include <cutils/properties.h>
#include <cutils/efuse_bch_8.h>

#include "common.h"
#include "roots.h"
#include "efuse.h"
#include "minzip/DirUtil.h"
#include "eeprom.h"

#define EFUSE_BYTES	8
#define EFUSE_MACLEN	17

const char *efuse_dev = "/dev/efuse";

static const char *SDCARD_AUDIO_LICENSE = "/sdcard/license.efuse";
static const char *SDCARD_AUDIO_LICENSE_OLD = "/sdcard/licence1.ef";

static const char *SDCARD_ETHERNET_MAC = "/sdcard/ethmac.efuse";
static const char *SDCARD_ETHERNET_MAC_OLD = "/sdcard/mac.ef";

static const char *SDCARD_HARDVERSION = "/sdcard/version.ver";

static const char *SDCARD_BLUETOOTH_MAC = "/sdcard/btmac.efuse";
static const char *SDCARD_BLUETOOTH_MAC_OLD = "/sdcard/btmac.ef";

static const char *SDCARD_HDCP = "/sdcard/hdcp.efuse";
static const char *SDCARD_HDCP2 = "/sdcard/hdcp2.efuse";

#ifdef EFUSE_LICENCE_ENABLE
#define EFUSE_MENU_MAX 3
#else
#define EFUSE_MENU_MAX 2
#endif /* EFUSE_LICENCE_ENABLE */

const char *efuse_items[EFUSE_MENU_MAX + 1] = {
#ifdef EFUSE_LICENCE_ENABLE
    "audio license", 
#endif /* EFUSE_LICENCE_ENABLE */
    "ethernet mac address",
    "bluetooth mac address", 
    NULL 
};

const int efuse_item_id[EFUSE_MENU_MAX + 1] = {
#ifdef EFUSE_LICENCE_ENABLE
    EFUSE_LICENCE, 
#endif /* EFUSE_LICENCE_ENABLE */
    EFUSE_MAC,
    EFUSE_MAC_BT,
    EFUSE_NONE 
};

const char *eeprom_items[EEPROM_MENU_MAX + 1] = {
	"Burn keys",
	"Clean keys",
	"Burn hardversion", 
	"Get hardversion",
	NULL 
};

const int eeprom_item_id[EEPROM_MENU_MAX + 1] = {
  	BURN_KEY,
  	CLEAN_KEY,
  	BURN_HARDVERSION,
  	GET_HARDVERSION,
	EEPROM_NONE 
};

const char wifi_name[WIFI_NAME_MAX][64]=
{
	"usb",
	"ap6330",
	"ap6181",
	"ap6210",
	"bcm40181",
	"bcm40183",
};

extern struct selabel_handle *sehandle;

extern FILE* fopen_path(const char *path, const char *mode) ;
extern void check_and_fclose(FILE *fp, const char *name);
extern const char** prepend_title(const char* const* headers);
extern int get_menu_selection(const char* const * headers, const char* const * items, int menu_only, int initial_selection, Device* device);

//add Rony for hdcp begin
#ifdef EFUSE_HDCP_ENABLE
#define EFUSE_HDCP_LEN 308
#define EFUSE_HDCP2_LEN 862
//static  int write_hdcp(int hdcpindex, RecoveryUI* ui);
//static  int write_hdcp2(int hdcpindex, RecoveryUI* ui);
//int verify_hdcp2_key(char *key, int size);
//int is_hdcp2_key_burn();
//static int index_match_mac(RecoveryUI* ui,char * line,unsigned char * efusemac);

#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */

#define blk0(i) (block->l[i] = (rol(block->l[i], 24) & 0xFF00FF00) | \
		(rol(block->l[i], 8) & 0x00FF00FF))
#define blk(i) (block->l[i & 15] = rol(block->l[(i + 13) & 15] ^ \
			block->l[(i + 8) & 15] ^ block->l[(i + 2) & 15] ^ block->l[i & 15], 1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) \
	z += ((w & (x ^ y)) ^ y) + blk0(i) + 0x5A827999 + rol(v, 5); \
w = rol(w, 30);
#define R1(v,w,x,y,z,i) \
	z += ((w & (x ^ y)) ^ y) + blk(i) + 0x5A827999 + rol(v, 5); \
w = rol(w, 30);
#define R2(v,w,x,y,z,i) \
	z += (w ^ x ^ y) + blk(i) + 0x6ED9EBA1 + rol(v, 5); w = rol(w, 30);
#define R3(v,w,x,y,z,i) \
	z += (((w | x) & y) | (w & x)) + blk(i) + 0x8F1BBCDC + rol(v, 5); \
w = rol(w, 30);
#define R4(v,w,x,y,z,i) \
	z += (w ^ x ^ y) + blk(i) + 0xCA62C1D6 + rol(v, 5); \
w=rol(w, 30);

/* Hash a single 512-bit block. This is the core of the algorithm. */
void SHA1Transform(DWORD *state, BYTE *buffer)
{
	DWORD a, b, c, d, e;
	typedef union {
		BYTE c[64];
		DWORD l[16];
	} CHAR64LONG16;
	CHAR64LONG16 *block;

	DWORD workspace[16];
	block = (CHAR64LONG16 *)workspace;
	memcpy(block, buffer, 64);

	/* Copy context->state[] to working vars */
	a = state[0];
	b = state[1];
	c = state[2];
	d = state[3];
	e = state[4];
	/* 4 rounds of 20 operations each. Loop unrolled. */
	R0(a,b,c,d,e, 0); R0(e,a,b,c,d, 1); R0(d,e,a,b,c, 2); R0(c,d,e,a,b, 3);
	R0(b,c,d,e,a, 4); R0(a,b,c,d,e, 5); R0(e,a,b,c,d, 6); R0(d,e,a,b,c, 7);
	R0(c,d,e,a,b, 8); R0(b,c,d,e,a, 9); R0(a,b,c,d,e,10); R0(e,a,b,c,d,11);
	R0(d,e,a,b,c,12); R0(c,d,e,a,b,13); R0(b,c,d,e,a,14); R0(a,b,c,d,e,15);
	R1(e,a,b,c,d,16); R1(d,e,a,b,c,17); R1(c,d,e,a,b,18); R1(b,c,d,e,a,19);
	R2(a,b,c,d,e,20); R2(e,a,b,c,d,21); R2(d,e,a,b,c,22); R2(c,d,e,a,b,23);
	R2(b,c,d,e,a,24); R2(a,b,c,d,e,25); R2(e,a,b,c,d,26); R2(d,e,a,b,c,27);
	R2(c,d,e,a,b,28); R2(b,c,d,e,a,29); R2(a,b,c,d,e,30); R2(e,a,b,c,d,31);
	R2(d,e,a,b,c,32); R2(c,d,e,a,b,33); R2(b,c,d,e,a,34); R2(a,b,c,d,e,35);
	R2(e,a,b,c,d,36); R2(d,e,a,b,c,37); R2(c,d,e,a,b,38); R2(b,c,d,e,a,39);
	R3(a,b,c,d,e,40); R3(e,a,b,c,d,41); R3(d,e,a,b,c,42); R3(c,d,e,a,b,43);
	R3(b,c,d,e,a,44); R3(a,b,c,d,e,45); R3(e,a,b,c,d,46); R3(d,e,a,b,c,47);
	R3(c,d,e,a,b,48); R3(b,c,d,e,a,49); R3(a,b,c,d,e,50); R3(e,a,b,c,d,51);
	R3(d,e,a,b,c,52); R3(c,d,e,a,b,53); R3(b,c,d,e,a,54); R3(a,b,c,d,e,55);
	R3(e,a,b,c,d,56); R3(d,e,a,b,c,57); R3(c,d,e,a,b,58); R3(b,c,d,e,a,59);
	R4(a,b,c,d,e,60); R4(e,a,b,c,d,61); R4(d,e,a,b,c,62); R4(c,d,e,a,b,63);
	R4(b,c,d,e,a,64); R4(a,b,c,d,e,65); R4(e,a,b,c,d,66); R4(d,e,a,b,c,67);
	R4(c,d,e,a,b,68); R4(b,c,d,e,a,69); R4(a,b,c,d,e,70); R4(e,a,b,c,d,71);
	R4(d,e,a,b,c,72); R4(c,d,e,a,b,73); R4(b,c,d,e,a,74); R4(a,b,c,d,e,75);
	R4(e,a,b,c,d,76); R4(d,e,a,b,c,77); R4(c,d,e,a,b,78); R4(b,c,d,e,a,79);
	/* Add the working vars back into context.state[] */
	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;
	/* Wipe variables */
	a = b = c = d = e = 0;
	memset(block, 0, 64);
}


/* SHA1Reset - Initialize new context */

void SHA1Reset(SHA1_CTX *context)
{
	/* SHA1 initialization constants */
	context->state[0] = 0x67452301;
	context->state[1] = 0xEFCDAB89;
	context->state[2] = 0x98BADCFE;
	context->state[3] = 0x10325476;
	context->state[4] = 0xC3D2E1F0;
	context->count[0] = context->count[1] = 0;
}


/* Run your data through this. */

void SHA1Input(SHA1_CTX* context, BYTE *_data, DWORD len)
{
	DWORD i, j;
	BYTE *data = _data;

	j = (context->count[0] >> 3) & 63;
	if ((context->count[0] += len << 3) < (len << 3))
		context->count[1]++;
	context->count[1] += (len >> 29);
	if ((j + len) > 63) {
		memcpy(&context->buffer[j], data, (i = 64-j));
		SHA1Transform(context->state, context->buffer);
		for ( ; i + 63 < len; i += 64) {
			SHA1Transform(context->state, &data[i]);
		}
		j = 0;
	}
	else i = 0;
	memcpy(&context->buffer[j], &data[i], len - i);

}


/* Add padding and return the message digest. */

void SHA1Result(SHA1_CTX *context, BYTE *digest)
{
	DWORD i;
	BYTE finalcount[8];

	for (i = 0; i < 8; i++) {
		finalcount[i] = (BYTE)
			((context->count[(i >= 4 ? 0 : 1)] >>
			  ((3-(i & 3)) * 8) ) & 255);  /* Endian independent */
	}
	SHA1Input(context, (BYTE *) "\200", 1);
	while ((context->count[0] & 504) != 448) {
		SHA1Input(context, (BYTE *) "\0", 1);
	}
	SHA1Input(context, finalcount, 8);  /* Should cause a SHA1Transform()
										 */
	for (i = 0; i < 20; i++) {
		digest[i] = (BYTE)
			((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) &
			 255);
	}
	/* Wipe variables */
	i = 0;
	memset(context->buffer, 0, 64);
	memset(context->state, 0, 20);
	memset(context->count, 0, 8);
	memset(finalcount, 0, 8);
}
/**************************************************************************
 * NOTES:       Test Vectors (from FIPS PUB 180-1) to verify implementation
 *              1- Input : "abc"
 *              Output : A9993E36 4706816A BA3E2571 7850C26C 9CD0D89D
 *              2- Input : "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
 *              Output : 84983E44 1C3BD26E BAAE4AA1 F95129E5 E54670F1
 *              2- Input : A million repetitions of 'a' - not applied (memory shortage)
 *              Output : 34AA973C D4C4DAA4 F61EEB2B DBAD2731 6534016F
 *              More test vectors can be obtained from FIPS web site
 ***************************************************************************/
void SHA1_Perform(BYTE *indata, DWORD inlen, BYTE *outdata) //calculate SHA-1 API
{
	SHA1_CTX sha;
	SHA1Reset(&sha);
	SHA1Input(&sha, indata, inlen);
	SHA1Result(&sha, outdata);
}
#endif /*EFUSE_HDCP_ENABLE*/

static int
efuse_opendev()
{
    int fd = open(efuse_dev, O_RDWR);
    if (fd < 0) 
        LOGE("efuse device not found\n");
    return fd;
}

static void
efuse_closedev(int fd)
{
    close(fd);
}

static size_t efuse_read(int efuse_type, char* result_buffer, size_t buffer_size, RecoveryUI* ui)
{
    loff_t ppos;
    size_t count;
    size_t read_size = 0;
    int fd = -1;
    efuseinfo_item_t efuseinfo_item;

    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[efuse_type];

    fd  = efuse_opendev();

    if (fd >= 0) {
        if (ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item))
            goto error;
        
        ppos = efuseinfo_item.offset;
        count = efuseinfo_item.data_len;

        if (buffer_size > count) {
            printf("error, buffer size not enough");
            goto error;
        }
        lseek(fd, ppos, SEEK_SET);
        read_size = read(fd, result_buffer, count);
        if(read_size != count)
            goto error;

        efuse_closedev(fd);
    }

    return read_size;
error:
    ui->Print("read efuse data %s error\n", efuse_title[efuse_type]); 
    if(fd >= 0)
        efuse_closedev(fd);
    return -1 ;
}

static size_t efuse_write(int efuse_type, unsigned char *data, size_t buffer_size, RecoveryUI* ui)
{
    size_t count;
    int ppos;
    size_t write_size = 0;
    int fd = -1;
    efuseinfo_item_t efuseinfo_item;

    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[efuse_type];

    fd  = efuse_opendev();

    if (fd >= 0) {
        if (ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            ui->Print("efuse ioctl error\n");
            goto error;
        }

        ppos = efuseinfo_item.offset;
        count = efuseinfo_item.data_len;

        ui->Print("efuse_write offset=%d, data_len=%d\n", ppos, count);

        if (buffer_size != count) {
            ui->Print("error, efuse data %s format is wrong\n", efuse_title[efuse_type]); 
            goto error;
        }
        if (lseek(fd, ppos, SEEK_SET) == -1)
            goto error;	
        write_size = write(fd, data, buffer_size);
        if (write_size != buffer_size) {
            ui->Print("error, efuse data %s write size wrong\n", efuse_title[efuse_type]); 
            goto error;	
        }
        efuse_closedev(fd);
    } else
    ui->Print("error,%s open file failed\n", efuse_title[efuse_type]); 

    return write_size;
    
error:
    if(fd >= 0)
       efuse_closedev(fd);
    return -1 ;
}

static int efuse_written_check(int efuse_type, RecoveryUI* ui)
{
    loff_t ppos;
    int count;
    size_t read_size = 0;
    int fd = -1, i, rc = 0;
    efuseinfo_item_t efuseinfo_item;
    char buffer[MAX_EFUSE_BYTES];

    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[efuse_type];

    fd  = efuse_opendev();
    if (fd >= 0) {
        if (ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            ui->Print("can't get efuse info\n"); 
            goto error;
        }

        ppos = efuseinfo_item.offset;
        count = efuseinfo_item.data_len;

        if (lseek(fd, ppos, SEEK_SET) == -1)
            goto error;
        
        memset(buffer, 0, MAX_EFUSE_BYTES);
        if(count != read(fd, buffer, count))
            goto error;	

        for (i = 0; i < count; i++) {
            if (buffer[i]) {
                rc = 1;
                ui->Print("this efuse segment has been written\n");
                break;
            }
        }

        efuse_closedev(fd);
    }

    return rc;
    
error:
    if(fd >= 0)
       efuse_closedev(fd);
    return -1 ;
}

/*
static int efuse_write_version(char* version_str,  RecoveryUI* ui)
{
    int rc = -1;
    unsigned char version_data[3];
    int version = -1, mach_id = -1;

    sscanf(version_str, "\"version=%d,mach_id=0x%x\"", &version, &mach_id);
    ui->Print("version=%x, mach_id=%x \n", version, mach_id);
    
    if(version == -1 || mach_id == -1)
        return -1;
    
    memset(version_data, 0, 3);
    version_data[0] = version & 0xff;
    version_data[1] = mach_id & 0xff;
    version_data[2] = (mach_id >> 8) & 0xff;
    
    if(3 == efuse_write(EFUSE_VERSION, version_data, 3, ui))
        rc = 0;

    return rc;
}
*/

#if defined(MESON3)
/**
  *  ---efuse_read_version 
  *  @version_str: save version(M3: 3 bytes) to this pointer
  *  @ui: recovery ui
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_version(char* version_str, RecoveryUI* ui)
{
    int rc = -1;
    char versionData[3];

    memset(versionData, 0, sizeof(versionData));
    memset(version_str, 0, 512);
	
    printf("%s:%d\n", __func__, __LINE__);
    if(3 == efuse_read(EFUSE_VERSION, versionData, 3, ui)) { 
        if(versionData[0] !=0x00) {
            rc = 0;				//have write version
            sprintf(version_str, "%02x:%02x:%02x", versionData[0],versionData[1],versionData[2]);
            printf("efuse read version success,version=%s\n", version_str);
        }
        else {
            rc = 1;				//haven't writen version
            printf("haven't write version before\n");
        }
    }	

    return rc;
}
#elif defined(MESON6) || defined(MESON8) 
/**
  *  ---efuse_read_version 
  *  @version_str: save version(M6: 1 byte) to this pointer
  *  @ui: recovery ui
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_version(char* version_str, RecoveryUI* ui)
{
    int rc = -1;
    char versionData[1] = {0};
    
    memset(version_str, 0, 512);

    printf("%s:%d\n", __func__, __LINE__);
    if(1 == efuse_read(EFUSE_VERSION, (char*)versionData, 1, ui)) {
        if(versionData[0] !=0x00) {
            rc = 0;				//have write version
            sprintf(version_str, "%02x", versionData[0]);
            printf("efuse read version success,version=%s\n", version_str); 
        }
        else {
            rc = 1;				//haven't writen version
            printf("haven't write version before\n");
        }
    }	

    return rc;
}
#endif

#if defined(MESON3) 
/**
  *  ---efuse_write_version
  *  @version_str: version(M3: 3 bytes)
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_version(char* version_str, RecoveryUI* ui)
{
    int i, data_size, rc = -1;
    int version = -1, mach_id[2] = {-1,-1};
    char *parg, *buf[50], buffer[100];
    unsigned char version_data[3];
    char version_read[3];

    printf("prepate to write version=%s\n", version_str);
    memset(version_data, 0, sizeof(version_data));
    memset(buffer, 0, sizeof(buffer));
    strcpy(buffer, version_str);

    if((parg = strtok(buffer, ":")) == NULL) {
        printf("get m3 version:%s failed.\n", version_str);
    }
    else {
        buf[0] = strdup(parg);
        for (i = 1; i < 100; i++) {
            if((parg = strtok(NULL, ":")) == NULL) {
                break;
            }
            else {
                buf[i] = strdup(parg);
            }
        }
    }

    data_size = i;
    printf("version[%d]={", data_size);
    for(i=0; i<data_size; i++) {
        version_data[i] = strtol(buf[i], NULL, 16);	
        if(i != data_size-1)
            printf("0x%02x,", version_data[i]);
        else
            printf("0x%02x", version_data[i]);
    }
    printf("}\n");

    version = version_data[0];
    mach_id[0] = version_data[1];
    mach_id[1] = version_data[2];
    if((version == -1) ||(mach_id[0] == -1) ||(mach_id[1] == -1)) {
        printf("m3 version wrong!: version=%d,mach_id[0]=%d,mach_id[1]=%d\n", version, mach_id[0], mach_id[1]);
        return -1;
    }	

    printf("%s:%d\n", __func__, __LINE__);
    if(3 == efuse_write(EFUSE_VERSION, version_data, 3, ui)) {
        rc = 0;
    }

    //write success,test efuse read
    if(rc == 0) {
        printf("efuse write version success,start to test efuse read version...\n");
        memset(version_read, 0, sizeof(version_read));
        if(3 == efuse_read(EFUSE_VERSION, version_read, 3, ui)) {
            if(!memcmp(version_data, version_read, 3)) {
                printf("test efuse read version success,read version=%02x:%02x:%02x\n", 
                        version_read[0], version_read[1], version_read[2]);
                rc = 0;
            }
            else {
                printf("test efuse read version success,read version=%02x:%02x:%02x,but not mach write\n", 
                        version_read[0], version_read[1], version_read[2]);
                rc = -1;
            }
        }
    }

    return rc;

}
#elif defined(MESON6) || defined(MESON8)
/**
  *  ---efuse_write_version
  *  @version_str: version(M6: 1 byte)
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_version(char* version_str, RecoveryUI* ui)
{
    int rc = -1, version = -1;
    unsigned char version_data[1];
    char version_read[1];

    printf("prepate to write version=%s \n", version_str);
    memset(version_data, 0, sizeof(version_data));

    version_data[0] = strtol(version_str, NULL, 16);
    version = version_data[0];

    if(version == -1) {
        printf("m6 version wrong!: version=%d\n",version);
        return -1;
    }

    printf("version[1]={0x%02x}\n", version_data[0]);
    printf("%s:%d\n", __func__, __LINE__);
    if(1 == efuse_write(EFUSE_VERSION, version_data, 1, ui)) {
        rc = 0;
    }	

    //write success,test efuse read
    if(rc == 0) {
        printf("efuse write version success,start to test efuse read version...\n");
        memset(version_read, 0, sizeof(version_read));
        if(1 == efuse_read(EFUSE_VERSION, version_read, 1, ui)) {
            if(!memcmp(version_data, version_read, 1)) {
                printf("test efuse read version success,read version=%02x\n", version_read[0]);
                rc = 0;
            }
            else {
                printf("test efuse read version success,read version=%02x,but not mach write\n", version_read[0]);
                rc = -1;
            }
        }
    }

    return rc;		
}
#endif

#ifndef SDCARD_EFUSE_MAC_ENABLE
/**
  *  ---efuse_read_mac
  *  @mac_str: save mac key datas to this pointer
  *  @type: mac type
  *  @ui: recovery ui pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_mac(char *mac_str, int type, RecoveryUI* ui)
{
    int i, flag = 0, rc = -1;
    char macData[6];

    memset(macData, 0, sizeof(macData));
    memset(mac_str, 0, 512);
    
    printf("%s:%d\n", __func__, __LINE__);
    if(6 == efuse_read(EFUSE_MAC, (char*)macData, 6, ui)) {
        for(i=0; i<6; i++) {
            if(macData[i] != 0x00) {
                flag = 1;
                break;
            }
        }

        if(flag) {
            rc = 0;				//have writen mac
            sprintf(mac_str, "%02x:%02x:%02x:%02x:%02x:%02x", 
                        macData[0], macData[1], macData[2], macData[3], macData[4], macData[5]);
            printf("efuse read mac success,mac=%s\n", mac_str);
        }
        else {
            rc = 1;				//haven't write mac
            printf("haven't write mac before\n");
        }
    }

    return rc;
}

/**
  *  ---efuse_write_mac
  *  @mac_str: mac key datas
  *  @type: mac type
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_mac(char *mac_str, int type, RecoveryUI* ui)
{
    int i, data_size, rc = -1;
    char *parg, *buf[50], buffer[100];
    char mac_data[6], mac_read[6];

    if(efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }

    printf("prepate to write mac=%s\n", mac_str);
    memset(mac_data, 0, sizeof(mac_data));
    strcpy(buffer, mac_str);
	
    if((parg = strtok(buffer, ":")) == NULL) {
        printf("get mac:%s failed.\n", mac_str);
    }
    else {
        buf[0] = strdup(parg);
        for (i = 1; i < 100; i++) {
            if((parg = strtok(NULL, ":")) == NULL) {
                break;
            }
            else {
                buf[i] = strdup(parg);
            }
        }
    }

    data_size = i;
    printf("mac[%d]={", data_size);
    for(i=0; i<data_size; i++) {
        mac_data[i] = strtol(buf[i], NULL, 16);	
        if(i != data_size-1)
            printf("0x%02x,", mac_data[i]);
        else
            printf("0x%02x", mac_data[i]);
    }
    printf("}\n");

    printf("%s:%d\n", __func__, __LINE__);  
    if(6 == efuse_write(EFUSE_MAC, (unsigned char *)mac_data, 6, ui)) {
        rc = 0;
    }
    
    //write success,test efuse read
    if(rc == 0) {
        printf("efuse write mac success,start to test efuse read mac...\n");
        memset(mac_read, 0, sizeof(mac_read));   
        if(6 == efuse_read(EFUSE_MAC, mac_read, 6, ui)) {
            if(!memcmp(mac_data, mac_read, 6)) {
                printf("test efuse read mac success,read mac=%02x:%02x:%02x:%02x:%02x:%02x\n",
                        mac_read[0], mac_read[1], mac_read[2], mac_read[3], mac_read[4], mac_read[5]);
                rc = 0;
            }
            else {
                printf("test efuse read mac success,read mac=%02x:%02x:%02x:%02x:%02x:%02x,but not mach write\n",
                        mac_read[0], mac_read[1], mac_read[2], mac_read[3], mac_read[4], mac_read[5]);
                rc = -1;
            }
        }
    }	

    return rc;
 }
#endif

/**
  *  ---efuse_read_mac_bt
  *  @mac_bt_str: save mac_bt key datas to this pointer
  *  @type: mac_bt type
  *  @ui: recovery ui pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_mac_bt(char *mac_bt_str, int type, RecoveryUI* ui)
{
    int i, flag = 0, rc = -1;
    char macbtData[6];

    memset(macbtData, 0, sizeof(macbtData));
    memset(mac_bt_str, 0, 512);
    
    printf("%s:%d\n", __func__, __LINE__);
    if(6 == efuse_read(EFUSE_MAC_BT, (char*)macbtData, 6, ui)) {
        for(i=0; i<6; i++) {
            if(macbtData[i] != 0x00) {
                flag = 1;
                break;
            }
        }

        if(flag) {
            rc = 0;				//have writen mac_bt
            sprintf(mac_bt_str, "%02x:%02x:%02x:%02x:%02x:%02x", 
                        macbtData[0], macbtData[1], macbtData[2], macbtData[3], macbtData[4], macbtData[5]);
            printf("efuse read mac_bt success,mac_bt=%s\n", mac_bt_str);
        }
        else {
            rc = 1;				//haven't write mac_bt	
            printf("haven't write mac_bt before\n");
        }
    }

    return rc;
}

/**
  *  ---efuse_write_mac_bt
  *  @mac_bt_str: mac_bt key datas
  *  @type: mac_bt type
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_mac_bt(char *mac_bt_str, int type, RecoveryUI* ui)
{
    int i, data_size, rc = -1;
    char *parg, *buf[50], buffer[100];
    char mac_bt_data[6], mac_bt_read[6];

    if(efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }

    printf("prepate to write mac_bt=%s\n", mac_bt_str);
    memset(mac_bt_data, 0, sizeof(mac_bt_data));
    strcpy(buffer, mac_bt_str);
	
    if((parg = strtok(buffer, ":")) == NULL) {
        printf("get mac:%s failed.\n", mac_bt_str);
    }
    else {
        buf[0] = strdup(parg);
        for (i = 1; i < 100; i++) {
            if((parg = strtok(NULL, ":")) == NULL) {
                break;
            }
            else {
                buf[i] = strdup(parg);
            }
        }
    }

    data_size = i;
    printf("mac_bt[%d]={", data_size);
    for(i=0; i<data_size; i++) {
        mac_bt_data[i] = strtol(buf[i], NULL, 16);	
        if(i != data_size-1)
            printf("0x%02x,", mac_bt_data[i]);
        else
            printf("0x%02x", mac_bt_data[i]);
    }
    printf("}\n");

    printf("%s:%d\n", __func__, __LINE__);  
    if(6 == efuse_write(EFUSE_MAC_BT, (unsigned char *)mac_bt_data, 6, ui)) {
        rc = 0;
    }	 

    //write success,test efuse read
    if(rc == 0) {
        printf("efuse write mac_bt success,start to test efuse read mac_bt...\n");
        memset(mac_bt_read, 0, sizeof(mac_bt_read));   
        if(6 == efuse_read(EFUSE_MAC_BT, mac_bt_read, 6, ui)) {
            if(!memcmp(mac_bt_data, mac_bt_read, 6)) {
                printf("test efuse read mac_bt success,read mac_bt=%02x:%02x:%02x:%02x:%02x:%02x\n",
                        mac_bt_read[0], mac_bt_read[1], mac_bt_read[2], mac_bt_read[3], mac_bt_read[4], mac_bt_read[5]);
                rc = 0;
            }
            else {
                printf("test efuse read mac_bt success,read mac_bt=%02x:%02x:%02x:%02x:%02x:%02x,but not mach write\n",
                        mac_bt_read[0], mac_bt_read[1], mac_bt_read[2], mac_bt_read[3], mac_bt_read[4], mac_bt_read[5]);
                rc = -1;
            }
        }
    }	

    return rc;
}

/**
  *  ---efuse_read_mac_wifi
  *  @mac_wifi_str: save mac_wifi key datas to this pointer
  *  @type: mac_wifi type
  *  @ui: recovery ui pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_mac_wifi(char *mac_wifi_str, int type, RecoveryUI* ui)
{
    int i, flag = 0, rc = -1;
    char macwifiData[6];

    memset(macwifiData, 0, sizeof(macwifiData));
    memset(mac_wifi_str, 0, 512);
    
    printf("%s:%d\n", __func__, __LINE__);
    if(6 == efuse_read(EFUSE_MAC_WIFI, (char*)macwifiData, 6, ui)) {
        for(i=0; i<6; i++) {
            if(macwifiData[i] != 0x00) {
                flag = 1;
                break;
            }
        }

        if(flag) {
            rc = 0;				//have writen mac_wifi
            sprintf(mac_wifi_str, "%02x:%02x:%02x:%02x:%02x:%02x", 
                        macwifiData[0], macwifiData[1], macwifiData[2], macwifiData[3], macwifiData[4], macwifiData[5]);
            printf("efuse read mac_wifi success,mac_wifi=%s\n", mac_wifi_str);
        }
        else {
            rc = 1;				//haven't write mac_wifi	
            printf("haven't write mac_wifi before\n");
        }
    }

    return rc;
}

/**
  *  ---efuse_write_mac_wifi
  *  @mac_wifi_str: mac_wifi key datas
  *  @type: mac_wifi type
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_mac_wifi(char *mac_wifi_str, int type, RecoveryUI* ui)
{
    int i, data_size, rc = -1;
    char *parg, *buf[50], buffer[100];
    char mac_wifi_data[6], mac_wifi_read[6];

    if(efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }

    printf("prepate to write mac_wifi=%s\n", mac_wifi_str);
    memset(mac_wifi_data, 0, sizeof(mac_wifi_data));
    strcpy(buffer, mac_wifi_str);
	
    if((parg = strtok(buffer, ":")) == NULL) {
        printf("get mac:%s failed.\n", mac_wifi_str);
    }
    else {
        buf[0] = strdup(parg);
        for (i = 1; i < 100; i++) {
            if((parg = strtok(NULL, ":")) == NULL) {
                break;
            }
            else {
                buf[i] = strdup(parg);
            }
        }
    }

    data_size = i;
    printf("mac_wifi[%d]={", data_size, data_size);
    for(i=0; i<data_size; i++) {
        mac_wifi_data[i] = strtol(buf[i], NULL, 16);	
        if(i != data_size-1)
            printf("0x%02x,", mac_wifi_data[i]);
        else
            printf("0x%02x", mac_wifi_data[i]);
    }
    printf("}\n");

    printf("%s:%d\n", __func__, __LINE__);  
    if(6 == efuse_write(EFUSE_MAC_WIFI, (unsigned char *)mac_wifi_data, 6, ui)) {
        rc = 0;
    }	 

    //write success,test efuse read
    if(rc == 0) {
        printf("efuse write mac_wifi success,start to test efuse read mac_wifi...\n");
        memset(mac_wifi_read, 0, sizeof(mac_wifi_read));   
        if(6 == efuse_read(EFUSE_MAC_WIFI, mac_wifi_read, 6, ui)) {
            if(!memcmp(mac_wifi_data, mac_wifi_read, 6)) {
                printf("test efuse read mac_wifi success,read mac_wifi=%02x:%02x:%02x:%02x:%02x:%02x\n",
                        mac_wifi_read[0], mac_wifi_read[1], mac_wifi_read[2], mac_wifi_read[3], mac_wifi_read[4], mac_wifi_read[5]);
                rc = 0;
            }
            else {
                printf("test efuse read mac_wifi success,read mac_wifi=%02x:%02x:%02x:%02x:%02x:%02x,but not mach write\n",
                        mac_wifi_read[0], mac_wifi_read[1], mac_wifi_read[2], mac_wifi_read[3], mac_wifi_read[4], mac_wifi_read[5]);
                rc = -1;
            }
        }
    }	

    return rc;
 }

/**
  *  ---efuse_read_usid
  *  @usid_str: save usid key datas to this pointer
  *  @type: usid type
  *  @ui: recovery ui pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_usid(char *usid_str, int type, RecoveryUI* ui)
{
    int i, usid_flag = 0, rc = -1;
    int fd = -1;
    size_t count = 0;
    char usidData[512];
    
    efuseinfo_item_t efuseinfo_item;
    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[EFUSE_USID];

    fd = efuse_opendev();
    if(fd >= 0) {
        if(ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            printf("efuse ioctl error\n");
            goto error;
        }
    }
    else {
        printf("efuse_opendev fail\n");
        goto error;
    }
    
    efuse_closedev(fd);
    count = efuseinfo_item.data_len;
    printf("usid max length is %d in efuse layout\n", count);

    printf("%s:%d\n", __func__, __LINE__);  
    memset(usid_str, 0, 512);
    memset(usidData, 0, sizeof(usidData));
    if(count == efuse_read(EFUSE_USID, usidData, count, ui)) {
        for(i=0; i<count; i++) {
            if(usidData[i] != 0x00) {
                usid_flag = 1;
                break;
            }
        }

        if(usid_flag) {
            rc = 0;				//have writen usid
            memcpy(usid_str, usidData, count);
            printf("efuse read usid success,usid=%s\n", usid_str);
        }
        else {
            rc = 1;				//haven't write usid	
            printf("haven't write usid before\n");
        }		
    }
	
    return rc;

error:
    printf("read efuse data %s error\n", efuse_title[EFUSE_USID]); 
    if(fd >= 0)
        efuse_closedev(fd);
    return -1 ;
}

/**
  *  ---efuse_write_usid
  *  @usid_str: usid key datas
  *  @type: usid type
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_usid(char *usid_str, int type, RecoveryUI* ui)
{
    int i, rc = -1;
    int fd = -1;
    size_t count = 0;
    char usid_data[512], usid_read[512];

    if(efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }
    
    memset(usid_data, 0, sizeof(usid_data));
    memcpy(usid_data, usid_str, strlen(usid_str));
    printf("prepare to write usid=%s\n", usid_data);
    
    efuseinfo_item_t efuseinfo_item;
    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[EFUSE_USID];
    
    fd  = efuse_opendev();
    if(fd >= 0) {
        if(ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            printf("efuse ioctl error\n");
            goto error;
        }
    }
    else {
        printf("efuse_opendev fail\n");
        goto error;
    }
    
    efuse_closedev(fd);
    count = efuseinfo_item.data_len;
    printf("usid max length is %d in efuse layout\n", count);

    printf("%s:%d\n", __func__, __LINE__);    
    if(count == efuse_write(EFUSE_USID, (unsigned char *)usid_data, count, ui)) {
        rc = 0;
        
        //write success,test efuse read
        printf("efuse write usid success,start to test efuse read usid...\n");
        memset(usid_read, 0, sizeof(usid_read));
        if(count == efuse_read(EFUSE_USID, (char*)usid_read, count, ui)) {
            if(!memcmp(usid_data, usid_read, count)) {
                printf("test efuse read usid success,read usid=%s\n", usid_read);
                rc = 0;     //write success,
            }
            else {
                printf("test efuse read usid success,read usid=%s,but not mach write\n", usid_read);
                rc = -1;    //if write success and read success, but read_data and write_data do not mach, then think to write fail
            }	
        }
        else
            rc = 0;         //if write success, but read fail, then think to write success
    }
    else
        rc = -1;            //write fail

    return rc;

error:
    printf("read efuse data %s error\n", efuse_title[EFUSE_USID]); 
    if(fd >= 0)
        efuse_closedev(fd);
    return -1 ;
}

/**
  *  ---efuse_read_hdcp
  *  @hdcp_str: save hdcp key datas to this pointer
  *  @type: hdcp type
  *  @ui: recovery ui pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int efuse_read_hdcp(char *hdcp_str, int type, RecoveryUI* ui)
{
    int i, hdcp_flag = 0, rc = -1;
    int fd = -1;
    size_t count = 0;
    char hdcpData[512];

    efuseinfo_item_t efuseinfo_item;
    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[EFUSE_HDCP];

    fd  = efuse_opendev();
    if(fd >= 0) {
        if(ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            printf("efuse ioctl error\n");
            goto error;
        }
    }
    else {
        printf("efuse_opendev fail\n");
        goto error;
    }
    
    efuse_closedev(fd);
    count = efuseinfo_item.data_len;
    printf("hdcp max length is %d in efuse layout\n", count);

    printf("%s:%d\n", __func__, __LINE__);   
    memset(hdcp_str, 0, 512);
    memset(hdcpData, 0, sizeof(hdcpData));
    if(count == efuse_read(EFUSE_HDCP, hdcpData, count, ui)) {			
        for(i=0; i<count; i++) {
            if(hdcpData[i] != 0x00) {
                hdcp_flag = 1;
                break;
            }
        }

        if(hdcp_flag) {
            rc = 0;				//have writen hdcp
            memcpy(hdcp_str, hdcpData, count);
            printf("efuse read hdcp success,hdcp is:\n");
            for(i=0; i<count; i++)
                printf("%02x:",hdcp_str[i]);
            printf("\n");
        }
        else {
            rc = 1;				//haven't write hdcp	
            printf("haven't write hdcp before\n");
        }		
    }
	
    return rc;
    
error:
    printf("read efuse data %s error\n", efuse_title[EFUSE_USID]); 
    if(fd >= 0)
        efuse_closedev(fd);
    return -1 ;
}

/**
  *  ---efuse_write_hdcp
  *  @hdcp_str: hdcp key datas(288 keys at present)
  *  @type: hdcp type
  *  @ui: recovery ui pointer
  *  return: 0->write success, -1->write failed
  */
int efuse_write_hdcp(char *hdcp_str, int type, RecoveryUI* ui)
{
    int i, rc = -1, fd = -1;
    int hdcp_key_len = 288;
    size_t count = 0;
    char hdcp_data[512], hdcp_read[512];
    
    if (efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }

    memset(hdcp_data, 0, sizeof(hdcp_data));
    memcpy(hdcp_data, hdcp_str, hdcp_key_len);  //copy 288 hdcp datas
    printf("prepare to write %d hdcp datas,hdcp is:\n", hdcp_key_len);
    for(i=0; i<hdcp_key_len; i++)
        printf("%02x:", hdcp_data[i]);
    printf("\n");	
	
    efuseinfo_item_t efuseinfo_item;
    memset(&efuseinfo_item, 0, sizeof(efuseinfo_item_t));
    efuseinfo_item.id = efuse_id[EFUSE_HDCP];
   
    fd  = efuse_opendev();
    if(fd >= 0) {
        if(ioctl(fd, EFUSE_INFO_GET, &efuseinfo_item)) {
            printf("efuse ioctl error\n");
            goto error;
        }
    }
    else {
        printf("efuse_opendev fail\n");
        goto error;
    }
    
    efuse_closedev(fd);
    count = efuseinfo_item.data_len;
    printf("hdcp max length is %d in efuse layout\n", count);
	
    printf("%s:%d\n", __func__, __LINE__);    
    if(count == efuse_write(EFUSE_HDCP, (unsigned char *)hdcp_data, count, ui)) {
        rc = 0;
        
        //write success,test efuse read
        printf("efuse write hdcp success,start to test efuse read hdcp...\n");
        memset(hdcp_read, 0, sizeof(hdcp_read));
        if(count == efuse_read(EFUSE_HDCP, (char*)hdcp_read, count, ui)) {
            if(!memcmp(hdcp_data, hdcp_read, count)) {
                printf("test efuse read hdcp success,read hdcp is:\n");
                for(i=0; i<count; i++)
                    printf("%02x:",hdcp_read[i]);
                printf("\n");	
                rc = 0;     //write success
            }
            else {
                printf("test efuse read hdcp success,but not mach write\n");
                rc = -1;    //if write success and read success, but read_data and write_data do not mach, then think to write fail
            }		
        }
        else
            rc = 0;	    //if write success, but read fail, then think to write success
    }
    else
        rc = -1;            //write fail

    return rc;

error:
    printf("read efuse data %s error\n", efuse_title[EFUSE_USID]); 
    if(fd >= 0)
        efuse_closedev(fd);
    return -1 ;
}


#define SECUKEY_BYTES 		512
#define PATH_VERSION       "/sys/class/aml_keys/aml_keys/version"
#define PATH_KEY_NAME		"/sys/class/aml_keys/aml_keys/key_name"
#define PATH_KEY_READ 		"/sys/class/aml_keys/aml_keys/key_read"  
#define PATH_KEY_WRITE     "/sys/class/aml_keys/aml_keys/key_write"

static char hex_to_asc(char para)                                                                                                           
{
    if(para>=0 && para<=9)
        para = para+'0';
    else if(para>=0xa && para<=0xf)
        para = para+'a'-0xa;
         
    return para;
 }
 
 static char asc_to_hex(char para)
 {
    if(para>='0' && para<='9')
        para = para-'0';
    else if(para>='a' && para<='f')
        para = para-'a'+0xa;
    else if(para>='A' && para<='F')
        para = para-'A'+0xa;
         
    return para;
 }

/**
  *  ---nand_write_version -- init nand
  *  @path: nand key nodes path in kernel
  *  @version_str: don't work at present
  *  return: 0->init success, 1->already inited, -1->init failed
  */
int secukey_inited = 0;
int nand_write_version(char *path, char *version_str)
{
    FILE *fp;
    int size, rc= -1;
    char *buff = "auto3";

    printf("%s:%d\n", __func__, __LINE__);
    if(secukey_inited) {
        printf("flash device already inited!!\n");
        return 1;
    }
    printf("should be inited first!\n");
    
    printf("path=%s\n", path);
    fp = fopen(path, "w");
    if (fp == NULL) {
        printf("no %s found\n", path);
        return -1;
    }

    if(fwrite(buff, 1, strlen(buff), fp) == strlen(buff)) {
        printf("fwrite(...fp) success\n");
        printf("init flash device ok!!\n");
        rc = 0;
        secukey_inited = 1;
    }	
    else {
        printf("fwrite(...fp) failed\n");
        rc = -1;
    }

    fclose(fp);
    return rc;
}

/**
  *  ---nand_read_mac
  *  @path: nand key nodes path in kernel
  *  @mac_str: save mac key datas to this pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int nand_read_mac(char *path, char *mac_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1, flag = 0;
    char *key_name = "mac", mac_read[2048], key_data[2048];

    printf("path=%s\n", path);
    memset(mac_read, 0, sizeof(mac_read));
    memset(key_data, 0, sizeof(key_data));
    memset(mac_str, 0, SECUKEY_BYTES);    

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        return -1;
    }
    if(fp1 != NULL) {
        fp2 = fopen(path, "r");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            fclose(fp1);
            return -1;
        }
    }
    
    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        fflush(fp1);
        count = fread(mac_read, 1, SECUKEY_BYTES*2, fp2);
        if(count >= 0) {
            printf("count=fread(...fp2)=%d\n", count);
            if(ferror(fp2)) {	
                printf("error reading from %s\n", path);
                clearerr(fp2);
            }

            for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                key_data[j] = (((asc_to_hex(mac_read[i]))<<4) | (asc_to_hex(mac_read[i+1])));
                i++;
            }

            for(i=0; i<SECUKEY_BYTES; i++) {
                if(key_data[i] !=0x00) {
                    flag = 1;
                    break;
                }
            }
            if(flag) {
                rc = 0;								//have writen mac
                memcpy(mac_str, key_data, SECUKEY_BYTES);
                printf("nand read mac success,mac=%s\n", mac_str);
            }
            else {
                rc = 1;								//haven't write mac
                printf("haven't write mac before\n");
            }
           
        }	
    }
    else
        printf("fwrite(...fp1) failed\n");

    if(NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if(NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_write_mac
  *  @path: nand key nodes path in kernel
  *  @mac_str: mac key datas
  *  return: 0->write success, -1->write failed
  */
int nand_write_mac(char *path, char *mac_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, mac_len=0, count, size, rc= -1;
    char *key_name = "mac";
    char key_data[2048], mac_data[2048], mac_read[2048];

    memset(key_data, 0, sizeof(key_data));
    memset(mac_data, 0, sizeof(mac_data));
    memset(mac_read, 0, sizeof(mac_read));
    
    mac_len =  strlen(mac_str);
    printf("path=%s\n", path);
    printf("prepare to write %d mac length,mac=%s\n", mac_len, mac_str);

    for(i=0,j=0; i<mac_len; i++,j++){
        mac_data[j]= hex_to_asc((mac_str[i]>>4) & 0x0f);
        mac_data[++j] = hex_to_asc((mac_str[i]) & 0x0f);
        printf("%02x:%02x:", mac_data[j-1], mac_data[j]);
    } 
    printf("\n");

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        goto error;
    }
    if(fp1 != NULL){
        fp2 = fopen(path, "w");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            goto error;
        }
    }
    
    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        if(fwrite(mac_data, 1, mac_len*2, fp2) == mac_len*2) {
            printf("fwrite(...fp2) success\n");
            rc = 0;

            fclose(fp1);
            fp1 = NULL;
            fclose(fp2);
            fp2 = NULL;
            
            //write success,test nand read
            printf("nand write mac success,start to test nand read mac...\n");
            fp1 = fopen(PATH_KEY_NAME, "w");
            if (fp1 == NULL) {
                printf("no %s found\n", PATH_KEY_NAME);
                goto error;
            }
            if(fp1 != NULL){
                fp2 = fopen(PATH_KEY_READ, "r");
                if(fp2 == NULL) {
                    printf("no %s found\n", PATH_KEY_READ);
                    goto error;
                }
            }
            if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
                printf("fwrite(...fp1) success\n");
                fflush(fp1);
                count = fread(mac_read, 1, SECUKEY_BYTES*2, fp2);
                if(count >= 0) {
                    printf("count=fread(...fp2)=%d\n", count);
                    if(ferror(fp2)) {
                        printf("error reading from %s\n", path);
                        clearerr(fp2);
                    }

                    for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                        key_data[j]= (((asc_to_hex(mac_read[i]))<<4) | (asc_to_hex(mac_read[i+1])));
                        i++;
                    }
                    printf("have writen mac data=%s\n", mac_str);
                    printf("test read mac data=%s\n", key_data);

                    if(!memcmp(key_data, mac_str, mac_len)) {
                        printf("test nand read mac success,read mac=%s\n", key_data);
                        rc = 0;
                    }
                    else {
                        printf("test nand read mac success,read mac=%s,but not mach write\n", key_data);
                        rc = -1;
                    }
                }	
            }
        }
        else {
            printf("fwrite(...fp2) failed\n");
            rc = -1;
        }
    }
    else
        printf("fwrite(...fp1) failed\n");

error:
    if (NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if (NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_read_mac_bt
  *  @path: nand key nodes path in kernel
  *  @mac_bt_str: save mac_bt key datas to this pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int nand_read_mac_bt(char *path, char *mac_bt_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1, flag = 0;
    char *key_name = "mac_bt", mac_bt_read[2048], key_data[2048];

    printf("path=%s\n", path);
    memset(mac_bt_read, 0, sizeof(mac_bt_read));
    memset(key_data, 0, sizeof(key_data));
    memset(mac_bt_str, 0, SECUKEY_BYTES);    

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        return -1;
    }
    if(fp1 != NULL) {
        fp2 = fopen(path, "r");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            fclose(fp1);
            return -1;
        }
    }

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        fflush(fp1);
        count = fread(mac_bt_read, 1, SECUKEY_BYTES*2, fp2);
        if(count >= 0) {
            printf("count=fread(...fp2)=%d\n", count);
            if(ferror(fp2)) {
                printf("error reading from %s\n", path);
                clearerr(fp2);
            }

            for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                key_data[j] = (((asc_to_hex(mac_bt_read[i]))<<4) | (asc_to_hex(mac_bt_read[i+1])));
                i++;
            }

            for(i=0; i<SECUKEY_BYTES; i++) {
                if(key_data[i] !=0x00) {
                    flag = 1;
                    break;
                }
            }
            if(flag) {
                rc = 0;								//have writen mac_bt
                memcpy(mac_bt_str, key_data, SECUKEY_BYTES);
                printf("nand read mac_bt success,mac_bt=%s\n", mac_bt_str);
            }
            else {
                rc = 1;								//haven't write mac_bt
                printf("haven't write mac_bt before\n");
            }
        }	
    }
    else
        printf("fwrite(...fp1) failed\n");

    if(NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if(NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_write_mac_bt
  *  @path: nand key nodes path in kernel
  *  @mac_bt_str: mac_bt key datas
  *  return: 0->write success, -1->write failed
  */
int nand_write_mac_bt(char *path, char *mac_bt_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, mac_bt_len=0, count, size, rc= -1;
    char *key_name = "mac_bt";
    char key_data[2048], mac_bt_data[2048], mac_bt_read[2048];

    memset(key_data, 0, sizeof(key_data));
    memset(mac_bt_data, 0, sizeof(mac_bt_data));
    memset(mac_bt_read, 0, sizeof(mac_bt_read));
    
    mac_bt_len =  strlen(mac_bt_str);
    printf("path=%s\n", path);
    printf("prepare to write %d mac_bt length,mac_bt=%s\n", mac_bt_len, mac_bt_str);

    for(i=0,j=0; i<mac_bt_len; i++,j++){
        mac_bt_data[j] = hex_to_asc((mac_bt_str[i]>>4) & 0x0f);
        mac_bt_data[++j] = hex_to_asc((mac_bt_str[i]) & 0x0f);
        printf("%02x:%02x:", mac_bt_data[j-1], mac_bt_data[j]);
    } 
    printf("\n");

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        goto error;
    }
    if(fp1 != NULL) {
        fp2 = fopen(path, "w");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            goto error;
        }
    }

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        if(fwrite(mac_bt_data, 1, mac_bt_len*2, fp2) == mac_bt_len*2) {
            printf("fwrite(...fp2) success\n");
            rc = 0;

            fclose(fp1);
            fp1 = NULL;
            fclose(fp2);
            fp2 = NULL;
            
            //write success,test nand read
            printf("nand write mac_bt success,start to test nand read mac_bt...\n");
            fp1 = fopen(PATH_KEY_NAME, "w");
            if (fp1 == NULL) {
                printf("no %s found\n", PATH_KEY_NAME);
                goto error;
            }
            if(fp1 != NULL){
                fp2 = fopen(PATH_KEY_READ, "r");
                if(fp2 == NULL) {
                    printf("no %s found\n", PATH_KEY_READ);
                    goto error;
                }
            }
            if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
                printf("fwrite(...fp1) success\n");
                fflush(fp1);
                count = fread(mac_bt_read, 1, SECUKEY_BYTES*2, fp2);
                if(count >= 0) {
                    printf("count=fread(...fp2)=%d\n", count);
                    if(ferror(fp2)) {
                        printf("error reading from %s\n", path);
                        clearerr(fp2);
                    }

                    for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                        key_data[j] = (((asc_to_hex(mac_bt_read[i]))<<4) | (asc_to_hex(mac_bt_read[i+1])));
                        i++;
                    }
                    printf("have writen mac_bt data=%s\n", mac_bt_str);
                    printf("test read mac_bt data=%s\n", key_data);

                    if(!memcmp(key_data, mac_bt_str, mac_bt_len)) {
                        printf("test nand read mac_bt success,read mac_bt=%s\n", key_data);
                        rc = 0;
                    }
                    else {
                        printf("test nand read mac_bt success,read mac_bt=%s,but not mach write\n", key_data);
                        rc = -1;
                    }
                }	
            }
        }
        else {
            printf("fwrite(...fp2) failed\n");
            rc = -1;
        }
    }
    else
        printf("fwrite(...fp1) failed\n");

error:
    if (NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if (NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_read_mac_wifi
  *  @path: nand key nodes path in kernel
  *  @mac_wifi_str: save mac_wifi key datas to this pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int nand_read_mac_wifi(char *path, char *mac_wifi_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1, flag = 0;
    char *key_name = "mac_wifi", mac_wifi_read[2048], key_data[2048];

    printf("path=%s\n", path);
    memset(mac_wifi_read, 0, sizeof(mac_wifi_read));
    memset(key_data, 0, sizeof(key_data));
    memset(mac_wifi_str, 0, SECUKEY_BYTES);    

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        return -1;
    }
    if(fp1 != NULL) {
        fp2 = fopen(path, "r");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            fclose(fp1);
            return -1;
        }
    }

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        fflush(fp1);
        count = fread(mac_wifi_read, 1, SECUKEY_BYTES*2, fp2);
        if(count >= 0) {
            printf("count=fread(...fp2)=%d\n", count);
            if(ferror(fp2)) {	
                printf("error reading from %s\n", path);
                clearerr(fp2);
            }
            
            for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                key_data[j] = (((asc_to_hex(mac_wifi_read[i]))<<4) | (asc_to_hex(mac_wifi_read[i+1])));
                i++;
            }

            for(i=0; i<SECUKEY_BYTES; i++) {
                if(key_data[i] !=0x00) {
                    flag = 1;
                    break;
                }
            }
            if(flag) {
                rc = 0;								//have writen mac_wifi
                memcpy(mac_wifi_str, key_data, SECUKEY_BYTES);
                printf("nand read mac_wifi success,mac_wifi=%s\n", mac_wifi_str);
            }
            else {
                rc = 1;								//haven't write mac_wifi
                printf("haven't write mac_wifi before\n");
            }
        }	
    }
    else
        printf("fwrite(...fp1) failed\n");

    if(NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if(NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_write_mac_wifi
  *  @path: nand key nodes path in kernel
  *  @mac_wifi_str: mac_wifi key datas
  *  return: 0->write success, -1->write failed
  */
int nand_write_mac_wifi(char *path, char *mac_wifi_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, mac_wifi_len=0, count, size, rc= -1;
    char *key_name = "mac_wifi";
    char key_data[2048], mac_wifi_data[2048], mac_wifi_read[2048];

    memset(key_data, 0, sizeof(key_data));
    memset(mac_wifi_data, 0, sizeof(mac_wifi_data));
    memset(mac_wifi_read, 0, sizeof(mac_wifi_read));
    
    mac_wifi_len =  strlen(mac_wifi_str);
    printf("path=%s\n", path);
    printf("prepare to write %d mac_wifi length,mac_wifi=%s\n", mac_wifi_len, mac_wifi_str);

    for(i=0,j=0; i<mac_wifi_len; i++,j++){
        mac_wifi_data[j] = hex_to_asc((mac_wifi_str[i]>>4) & 0x0f);
        mac_wifi_data[++j] = hex_to_asc((mac_wifi_str[i]) & 0x0f);
        printf("%02x:%02x:", mac_wifi_data[j-1], mac_wifi_data[j]);
    } 
    printf("\n");

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        goto error;
    }
    if(fp1 != NULL) {
        fp2 = fopen(path, "w");
        if (fp2 == NULL) {
            printf("no %s found\n", path);
            goto error;
        }
    }

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        if(fwrite(mac_wifi_data, 1, mac_wifi_len*2, fp2) == mac_wifi_len*2) {
            printf("fwrite(...fp2) success\n");
            rc = 0;

            fclose(fp1);
            fp1 = NULL;
            fclose(fp2);
            fp2 = NULL;
            
            //write success,test nand read
            printf("nand write mac_wifi success,start to test nand read mac_wifi...\n");
            fp1 = fopen(PATH_KEY_NAME, "w");
            if (fp1 == NULL) {
                printf("no %s found\n", PATH_KEY_NAME);
                goto error;
            }
            if(fp1 != NULL){
                fp2 = fopen(PATH_KEY_READ, "r");
                if(fp2 == NULL) {
                    printf("no %s found\n", PATH_KEY_READ);
                    goto error;
                }
            }
            if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
                printf("fwrite(...fp1) success\n");
                fflush(fp1);
                count = fread(mac_wifi_read, 1, SECUKEY_BYTES*2, fp2);
                if(count >= 0) {
                    printf("count=fread(...fp2)=%d\n", count);
                    if(ferror(fp2)) {
                        printf("error reading from %s\n", path);
                        clearerr(fp2);
                    }

                    for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                        key_data[j] = (((asc_to_hex(mac_wifi_read[i]))<<4) | (asc_to_hex(mac_wifi_read[i+1])));
                        i++;
                    }
                    printf("have writen mac_wifi data=%s\n", mac_wifi_str);
                    printf("test read mac_wifi data=%s\n", key_data);

                    if(!memcmp(key_data, mac_wifi_str, mac_wifi_len)) {
                        printf("test nand read mac_wifi success,read mac_wifi=%s\n", key_data);
                        rc = 0;
                    }
                    else {
                        printf("test nand read mac_wifi success,read mac_wifi=%s,but not mach write\n", key_data);
                        rc = -1;
                    }
                }	
            }
        }
        else {
            printf("fwrite(...fp2) failed\n");
            rc = -1;
        }
    }
    else
        printf("fwrite(...fp1) failed\n");

error:
    if (NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if (NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }

    return rc;
}

/**
  *  ---nand_read_usid
  *  @path: nand key nodes path in kernel
  *  @usid_str: save usid key datas to this pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int nand_read_usid(char *path, char *usid_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1, usid_flag = 0;
    char *key_name = "usid", key_data[2048], usid_read[2048];

    printf("path=%s\n", path);
    memset(key_data, 0, sizeof(key_data));
    memset(usid_read, 0, sizeof(usid_read));
    memset(usid_str, 0, SECUKEY_BYTES);  
    
    fp1 = fopen(PATH_KEY_NAME, "w");
    if (fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        return -1;
    }
    if(fp1 != NULL){
        fp2 = fopen(path, "r");
        if(fp2 == NULL) {
            printf("no %s found\n", path);
            fclose(fp1);
            return -1;
        }	
    }	

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        fflush(fp1);
        count = fread(usid_read, 1, SECUKEY_BYTES*2, fp2);								
        if(count >= 0) {
            printf("count=fread(...fp2)=%d\n",count);
            if(ferror(fp2)) {	
                printf("error reading from %s\n",path); 
                clearerr(fp2);
            }	
            
            for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){                                                                     
                key_data[j] = (((asc_to_hex(usid_read[i]))<<4) | (asc_to_hex(usid_read[i+1])));
                i++;
            }
            
            for(i=0; i<SECUKEY_BYTES; i++) {
                if(key_data[i] !=0x00) {	
                    usid_flag = 1;
                    break;
                }
            }	
            if(usid_flag) {
                rc = 0;								//have writen usid
                memcpy(usid_str, key_data, SECUKEY_BYTES);
                printf("nand read usid success,usid=%s\n", usid_str);
            }	
            else {
                rc = 1;								//haven't write usid	
                printf("haven't write usid before\n");
            }
        }										
    }	
    else
        printf("fwrite(...fp1) failed\n");	
	
	if (NULL != fp2)
	{
		fclose(fp2);
		fp2 = NULL;
	}
	if (NULL != fp1)
	{
		fclose(fp1);
		fp1 = NULL;
	}
	
	return rc;

}

/**
  *  ---nand_write_usid
  *  @path: nand key nodes path in kernel
  *  @usid_str: usid key datas
  *  return: 0->write success, -1->write failed
  */
int nand_write_usid(char *path, char *usid_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, usid_len=0, count, size, rc= -1;
    char *key_name = "usid";
    char key_data[2048], usid_data[2048], usid_read[2048];

    memset(key_data, 0, sizeof(key_data));
    memset(usid_data, 0, sizeof(usid_data));
    memset(usid_read, 0, sizeof(usid_read));

    usid_len = strlen(usid_str);
    printf("path=%s\n", path);		
    printf("prepare to write %d usid length,usid=%s\n", usid_len, usid_str);

    for(i=0,j=0; i<usid_len; i++,j++){
        usid_data[j] = hex_to_asc((usid_str[i]>>4) & 0x0f);
        usid_data[++j] = hex_to_asc((usid_str[i]) & 0x0f);
        printf("%02x:%02x:", usid_data[j-1], usid_data[j]);
    } 
    printf("\n");

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        goto error;
    }

    if(fp1 != NULL){
        fp2 = fopen(path, "w");
        if(fp2 == NULL) {
            printf("no %s found\n", path);
            goto error;
        }	
    }	

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        if(fwrite(usid_data, 1, usid_len*2, fp2) == usid_len*2) {
            printf("fwrite(...fp2) success\n");
            rc = 0;                     //write success
			
            fclose(fp1);
            fp1 = NULL;
            fclose(fp2);
            fp2 = NULL;

            //write success,test nand read
            printf("nand write usid success,start to test nand read usid...\n");
            fp1 = fopen(PATH_KEY_NAME, "w");
            if (fp1 == NULL) {
                printf("no %s found\n", PATH_KEY_NAME);
                goto error;
            }
            if(fp1 != NULL) {
                fp2 = fopen(PATH_KEY_READ, "r");
                if (fp2 == NULL) {
                    printf("no %s found\n", PATH_KEY_READ);
                    goto error;
                }	
            }
            if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
                printf("fwrite(...fp1) success\n");
                fflush(fp1);
                count = fread(usid_read, 1, SECUKEY_BYTES*2, fp2);  
                if(count >= 0) {
                    printf("count=fread(...fp2)=%d\n", count);		
                    if(ferror(fp2)) {
                        printf("error reading from %s\n", path); 
                        clearerr(fp2);
                    }

                    for (i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                        key_data[j] = (((asc_to_hex(usid_read[i]))<<4) | (asc_to_hex(usid_read[i+1])));
                        i++;
                    }
                    printf("have writen usid data=%s\n", usid_str);	
                    printf("test read usid data=%s\n", key_data);

                    if(!memcmp(key_data, usid_str, usid_len)) {
                        printf("test nand read usid success,read usid=%s\n", key_data);
                        rc = 0;
                    }
                    else {
                        printf("test nand read usid success,read usid=%s,but not mach write\n", key_data);
                        rc = -1;    //if write success and read success, but read_data and write_data do not mach, then think to write fail
                    }			
                }			
            }
        }	
        else {
            printf("fwrite(...fp2) failed\n");
            rc = -1;
        }	
    }
    else
        printf("fwrite(...fp1) failed\n");

error:
    if(NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if(NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }
	
    return rc;
}

/**
  *  ---nand_read_hdcp
  *  @path: nand key nodes path in kernel
  *  @hdcp_str: save hdcp key datas to this pointer
  *  return: 0->have writen before, 1->haven't write before, -1->read failed
  */
int nand_read_hdcp(char *path, char *hdcp_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1, hdcp_flag = 0;
    char *key_name = "hdcp", key_data[2048], hdcp_read[2048];

    printf("path=%s\n", path);
    memset(key_data, 0, sizeof(key_data));
    memset(hdcp_read, 0, sizeof(hdcp_read));
    memset(hdcp_str, 0, SECUKEY_BYTES);  
    
    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        return -1;
    }
    if(fp1 != NULL){
        fp2 = fopen(path, "r");
        if(fp2 == NULL) {
            printf("no %s found\n", path);
            fclose(fp1);
            return -1;
        }	
    }	
    
    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        fflush(fp1);
        count = fread(hdcp_read,  1, SECUKEY_BYTES*2, fp2);
        if(count >= 0) {
            printf("count=fread(...fp2)=%d\n",count);
            if(ferror(fp2)) {
                printf("error reading from %s\n",path); 
                clearerr(fp2);
            }
            
            for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){                                                                     
                key_data[j] = (((asc_to_hex(hdcp_read[i]))<<4) | (asc_to_hex(hdcp_read[i+1])));
                i++;
            }

            for(i=0; i<SECUKEY_BYTES; i++) {
                if(key_data[i] !=0x00) {	
                    hdcp_flag = 1;
                    break;
                }
            }	
            if(hdcp_flag) {
                rc = 0;								//have writen hdcp
                memcpy(hdcp_str, key_data, SECUKEY_BYTES);
                printf("nand read hdcp success,hdcp is:\n");	
                for(i=0; i<SECUKEY_BYTES; i++)
                    printf("%02x:", key_data[i]);
                printf("\n");
            }	
            else {
                rc = 1;								//haven't write hdcp	
                printf("haven't write hdcp before\n");
            }
        }				
    }	
    else
        printf("fwrite(...fp1) failed\n");	
	
    if (NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if (NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }
	
    return rc;
}

/**
  *  ---nand_write_hdcp
  *  @path: nand key nodes path in kernel
  *  @hdcp_str: hdcp key datas(288 keys at present)
  *  return: 0->write success, -1->write failed
  */
int nand_write_hdcp(char *path, char *hdcp_str)
{
    FILE *fp1=NULL, *fp2=NULL;
    int i, j, count, size, rc= -1;
    char *key_name = "hdcp";
    char key_data[2048], hdcp_data[2048], hdcp_read[2048];
    int hdcp_key_len = 288;
    
    printf("path=%s\n", path);	
    memset(key_data, 0, sizeof(key_data));
    memset(hdcp_data, 0, sizeof(hdcp_data));
    memset(hdcp_read, 0, sizeof(hdcp_read));

    printf("prepare to write %d hdcp datas,hdcp is:\n", hdcp_key_len);
    for(i=0; i<hdcp_key_len; i++)
         printf("%02x:", hdcp_str[i]);
    printf("\n");
    
    for (i=0,j=0; i<hdcp_key_len; i++,j++){
        hdcp_data[j] = hex_to_asc((hdcp_str[i]>>4) & 0x0f);
        hdcp_data[++j] = hex_to_asc((hdcp_str[i]) & 0x0f);
        printf("%02x:%02x:", hdcp_data[j-1], hdcp_data[j]);
    } 
    printf("\n");

    fp1 = fopen(PATH_KEY_NAME, "w");
    if(fp1 == NULL) {
        printf("no %s found\n", PATH_KEY_NAME);
        goto error;
    }
    if(fp1 != NULL){
        fp2 = fopen(path, "w");
        if(fp2 == NULL) {
            printf("no %s found\n", path);
            goto error;
        }	
    }	

    printf("%s:%d\n", __func__, __LINE__);
    if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
        printf("fwrite(...fp1) success\n");
        if(fwrite(hdcp_data, 1, hdcp_key_len*2, fp2) == hdcp_key_len*2)	{
            printf("fwrite(...fp2) success\n");
            rc = 0;                     //write success
			
            fclose(fp1);
            fp1 = NULL;
            fclose(fp2);
            fp2 = NULL;
            
            //write success,test nand read
            printf("nand write hdcp success,start to test nand read hdcp...\n");
            fp1 = fopen(PATH_KEY_NAME, "w");
            if (fp1 == NULL) {
                printf("no %s found\n", PATH_KEY_NAME);
                goto error;
            }
            if(fp1 != NULL){
                fp2 = fopen(PATH_KEY_READ, "r");
                if(fp2 == NULL) {
                    printf("no %s found\n", PATH_KEY_READ);
                    goto error;
                }	
            }
            if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name)) {
                printf("fwrite(...fp1) success\n");
                fflush(fp1);
                count = fread(hdcp_read, 1, SECUKEY_BYTES*2, fp2);  
                if(count >= 0) {
                    printf("count=fread(...fp2)=%d\n",count);		
                    if(ferror(fp2)) {
                        printf("error reading from %s\n",path); 
                        clearerr(fp2);
                    }
                    
                    for(i=0,j=0; i<SECUKEY_BYTES*2; i++,j++){
                        key_data[j] = (((asc_to_hex(hdcp_read[i]))<<4) | (asc_to_hex(hdcp_read[i+1])));
                        i++;
                    }

                    if(!memcmp(key_data, hdcp_str, hdcp_key_len)) {
                        printf("test nand read hdcp success,read hdcp is:\n");
                        for(i=0; i<hdcp_key_len; i++)
                            printf("%02x:", key_data[i]);
                        printf("\n");
                        rc = 0;
                    }
                    else {
                        printf("test nand read hdcp success,but not mach write\n");
                        rc = -1;        //if write success and read success, but read_data and write_data do not mach, then think to write fail
                    }								
                }
            }
        }	
        else {
            printf("fwrite(...fp2) failed\n");
            rc = -1;
        }	
    }
    else
        printf("fwrite(...fp1) failed\n");

error:
    if (NULL != fp2) {
        fclose(fp2);
        fp2 = NULL;
    }
    if (NULL != fp1) {
        fclose(fp1);
        fp1 = NULL;
    }
	
    return rc;
}

//Rony add
#ifdef EFUSE_HDCP_ENABLE
char i_to_asc(char para)                                                                                                           
 {
     if(para>=0 && para<=9)
         para = para+'0';
     else if(para>=0xa && para<=0xf)
         para = para+'a'-0xa;
         
         return para;
 }
 
 char asc_to_i(char para)
 {
     if(para>='0' && para<='9')
         para = para-'0';
     else if(para>='a' && para<='f')
         para = para-'a'+0xa;
     else if(para>='A' && para<='F')
         para = para-'A'+0xa;
         
         return para;
 }
 
int nand_read(char *name, char *path, char *key_data, int size)
{
	FILE *fp1=NULL, *fp2=NULL;
 	int i, j, count, rc= 0, hdcp_flag = 0;
	char *key_name = name;
	char buf[size*2];

	
	printf("path=%s\n", path);
	memset(buf, 0, size*2);

	fp1 = fopen(PATH_KEY_NAME, "w");
	if (fp1 == NULL) {
		printf("no %s found\n", PATH_KEY_NAME);
		return -1;
	}

	if(fp1 != NULL){
		fp2 = fopen(path, "r");
		if (fp2 == NULL) {
			printf("no %s found\n", path);
			fclose(fp1);
			return -1;
		}	
	}	

	if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name))
	{
		printf("fwrite(...fp1) success\n");
		fflush(fp1);
		count = fread(buf,  1, size*2, fp2);
		if(count >= 0)									//count value is the number of actual read
		{
			printf("count=fread(...fp2)=%d\n",count);
			if(ferror(fp2))								//error has occurred
			{
				printf("error reading from %s\n",path); 
				clearerr(fp2);
			}

			//convert in nand
			for (i=0,j=0; i<size*2; i++,j++){                                                                     
				key_data[j]= (((asc_to_i(buf[i]))<<4) | (asc_to_i(buf[i+1])));
				i++;
			}
			
			printf("read %s data=", name);	
			for(i=0; i<size; i++)
				printf("%.2x:",key_data[i]);
			printf("\n");

			for(i=0; i<size; i++)
			{
				if(key_data[i] != 0x00)	
				{
					hdcp_flag = 1;
					break;
				}
			}	
			if(hdcp_flag)
				rc = 0;								//have writen hdcp
			else
				rc = -1;								//haven't write hdcp	
		}			
		
	}	
	else
		printf("fwrite(...fp1) failed\n");	
	
	if (NULL != fp2)
	{
		fclose(fp2);
		fp2 = NULL;
	}
	if (NULL != fp1)
	{
		fclose(fp1);
		fp1 = NULL;
	}
	
	return rc;
}

int nand_write(char *name, char *path, char *hdcp_str, int size)
{
	FILE *fp1=NULL, *fp2=NULL;
 	int i, j, count, rc= 0;
	char *key_name = name;
	char key_data[4096], hdcp_data[4096];
	
	memset(key_data, 0, sizeof(key_data));
	memset(hdcp_data, 0, sizeof(hdcp_data));
 	printf("path=%s\n", path);	
	//convert in nand
	for (i=0,j=0; i<size; i++,j++){
		hdcp_data[j]= i_to_asc((hdcp_str[i]>>4) & 0x0f);
		hdcp_data[++j]= i_to_asc((hdcp_str[i]) & 0x0f);
		printf("%02x:%02x:", hdcp_data[j-1], hdcp_data[j]);
	 } 
	printf("\n");

	fp1 = fopen(PATH_KEY_NAME, "w");
	if (fp1 == NULL) {
		printf("no %s found\n", PATH_KEY_NAME);
		goto error;
	}

	if(fp1 != NULL){
		fp2 = fopen(path, "w");
		if (fp2 == NULL) {
			printf("no %s found\n", path);
			goto error;
		}	
	}	


	if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name))
	{
		printf("fwrite(...fp1) success\n");
		if(fwrite(hdcp_data,  1, size*2, fp2) == size*2)										
		{
			printf("fwrite(...fp2) success\n");
			rc = 0;									//write sucess
			
			fclose(fp1);
			fp1 = NULL;
			fclose(fp2);
			fp2 = NULL;		

			//##########test read hdcp
			printf("#####nand write %s success,now test nand read %s:\n", name, name);
			fp1 = fopen(PATH_KEY_NAME, "w");
			if (fp1 == NULL) {
				printf("no %s found\n", PATH_KEY_NAME);
				rc = -1;
				goto error;
			}
			if(fp1 != NULL){
				fp2 = fopen(PATH_KEY_READ, "r");
				if (fp2 == NULL) {
					printf("no %s found\n", PATH_KEY_READ);
					rc = -1;
					goto error;
				}	
			}

			if(fwrite(key_name, 1, strlen(key_name), fp1) == strlen(key_name))
			{
				printf("fwrite(...fp1) success\n");
				fflush(fp1);
				count = fread(key_data,  1, size*2, fp2);  
				if(count >= 0)							//count value is the number of actual read	
				{
					printf("count=fread(...fp2)=%d\n",count);		
					if(ferror(fp2))						//error has occurred
					{
						printf("error reading from %s\n",path); 
						clearerr(fp2);
					}

					//convert in nand
					for (i=0,j=0; i<size*2; i++,j++){
						key_data[j]= (((asc_to_i(key_data[i]))<<4) | (asc_to_i(key_data[i+1])));
						i++;
					}
					
					printf("writen hdcp data=");	
					for(i=0; i<size; i++)
						printf("%.2x:",hdcp_str[i]);
					printf("\n");
					printf("read hdcp data=");	
					for(i=0; i<size; i++)
						printf(":%02x", key_data[i]);
					printf("\n");
					
					for(i=0; i<size; i++)
					{
						if(key_data[i] != hdcp_str[i])
						{
							printf("hdcp writen & read data not mach\n");
							rc = -1;					//if write success and read success, but read_data and write_data do not mach, then think to write fail
							break;
						}
					}						
				}
			}
		}	
		else
		{
			printf("fwrite(...fp2) failed\n");
			rc = -1;
		}	
	}
	else
		printf("fwrite(...fp1) failed\n");

error:
	if (NULL != fp2)
	{
		fclose(fp2);
		fp2 = NULL;
	}
	if (NULL != fp1)
	{
		fclose(fp1);
		fp1 = NULL;
	}
	
	return rc;
}

int verify_hdcp2_key(char *key, int size)
{
	char *sha_ptr = key;
	int i, flag = 1;
	SHA1_CTX sha;
	char  Message_Digest[20];
	sha_ptr += (size - 20);

	LOGW("receive hdcp2_verify_data=");
	for(i=0; i<20; i++)
		printf("%.2x:", sha_ptr[i]);
	printf("\n");

	LOGW("start to verify_hdcp2_key data...\n");
	SHA1Reset(&sha);
	SHA1Input(&sha, (unsigned char*)key, (size - 20));
	SHA1Result(&sha, (unsigned char*)Message_Digest);

	LOGW("calculate hdcp_verify_data=");
	for(i=0; i<20; i++)
		printf("%.2x:", Message_Digest[i]);
	printf("\n");

	for(i=0; i<20; i++)
	{
		if(sha_ptr[i] != Message_Digest[i])
		{
			flag = 0;
			break;
		}
	}
	
	if(flag == 0)
		return 0;

	printf("verify hdcp2 key ok!\n");
	return 1;
}

static int hdcp_ksv_valid(char * dat){	
	int i, j, one_num = 0;	
	for(i = 0; i < 5; i++){		
		for(j=0;j<8;j++) {			
			if((dat[i]>>j)&0x1) {				
				one_num++;			
			}		
		}	
	}	
	LOGW("0x%02x 0x%02x 0x%02x 0x%02x 0x%02x one_num=%d\n",dat[0],dat[1],dat[2],dat[3],dat[4] ,one_num);	
	return (one_num == 20);
}

int is_hdcp2_key_burn()
{
	int i, j, ret = 0;
	unsigned char key_data[4096];
	printf("============ check hdcp2 key is burn\n");
	nand_write_version(PATH_VERSION, NULL);
	ret = nand_read("hdcp2lc128", PATH_KEY_READ, (char *)key_data, 36);
	if(ret)
		return 0;		

	//convert in nand
	//for (i=0,j=0; i<36*2; i++,j++){
	//	key_data[j]= (((asc_to_i(key_data[i]))<<4) | (asc_to_i(key_data[i+1])));
	//	i++;
	//}
	
	if(verify_hdcp2_key((char *)key_data, 36) == 0)
		return 0;
	
	ret = nand_read("hdcp2key", PATH_KEY_READ, (char *)key_data, 862);
	if(ret)
		return 0;		

	//convert in nand
	//for (i=0,j=0; i<862*2; i++,j++){
	//	key_data[j]= (((asc_to_i(key_data[i]))<<4) | (asc_to_i(key_data[i+1])));
	//	i++;
	//}
	
	if(verify_hdcp2_key((char *)key_data, 862) == 0)
		return 0;

	return 1;
}

static  int write_hdcp(int hdcpindex, RecoveryUI* ui)
{
	const char *path = SDCARD_HDCP;
	char buf[5];
	FILE* fp = NULL;
	int size = 0;
	int i;
	int ret = 0;
	int  writeHdcp_flag = 1,index = 0;

	unsigned char hdcp[EFUSE_HDCP_LEN];
	char efuse_data[512], hdcp_verify_data[512];

	memset(efuse_data,0,sizeof(efuse_data));
	memset(hdcp_verify_data, 0, sizeof(hdcp_verify_data));


	fp = fopen(path, "rb");
	if(fp == NULL)
	{
		printf("fopen fail\n");
		ui->Print("error: the  hdcp.efuse file  read fail\n");
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	if(0 != size % 308)
	{
		ui->Print("error: the  hdcp.efuse file wrong\n");
		fclose(fp);
		return -1;
	}
	if(size / 308 < hdcpindex){
		ui->Print("error: the hdcp.efuse file not key to use\n");
		fclose(fp);
		return -1;
	}

	size = EFUSE_HDCP_LEN;

	LOGW("sdcard_efuse_write_hdcp() path=%s  size=%d  hdcpindex=%d\n", path, size,hdcpindex);
	fseek(fp, (long)hdcpindex * EFUSE_HDCP_LEN, SEEK_SET);

	memset((void*)hdcp, 0, EFUSE_HDCP_LEN);
	if((int)fread(hdcp, 1, size, fp) != size)
	{
		fclose(fp);
		ui->Print("error: the hdcp.efuse file read fail\n");
		return -1;
	}
	fclose(fp);

	memcpy(efuse_data,hdcp,288);
	memcpy(hdcp_verify_data,hdcp+288,20);

	LOGW("receive hdcp_data=");
	for(i=0; i<288; i++)
		printf("%.2x:", efuse_data[i]);
	printf("\n");

	LOGW("receive hdcp_verify_data=");
	for(i=0; i<20; i++)
		printf("%.2x:", hdcp_verify_data[i]);
	printf("\n");

	SHA1_CTX sha;
	BYTE  Message_Digest[20];
	hdcp_llc_file *llc_key;
	memset(Message_Digest, 0, sizeof(Message_Digest));
	llc_key = (hdcp_llc_file *)efuse_data;

	LOGW("start to verify hdcp data...\n");
	SHA1Reset(&sha);
	SHA1Input(&sha, (unsigned char*)llc_key, 288);
	SHA1Result(&sha, Message_Digest);

	LOGW("calculate hdcp_verify_data=");
	for(i=0; i<20; i++)
		LOGW("%.2x:", Message_Digest[i]);

	for(i=0; i<20; i++)
	{
		if(hdcp_verify_data[i] != Message_Digest[i])
		{
			writeHdcp_flag = 0;
			break;
		}
	}
	if(writeHdcp_flag)          //verify success,hdcp can write
	{
		nand_write_version(PATH_VERSION, NULL);
		ret = nand_write_hdcp(PATH_KEY_WRITE, (char *)hdcp);
	}
	else
		ret = -1;
	return ret;
}

static  int write_hdcp2(int hdcpindex, RecoveryUI* ui)
{
	const char *path = SDCARD_HDCP2;
	char buf[5];
	FILE* fp = NULL;
	long index;
	int size = 0;
	int i, lc128_size, hdcp2_size;
	int ret = 0;

	unsigned char hdcp2_key[EFUSE_HDCP2_LEN];
	unsigned char lc128_key[36];

	memset(hdcp2_key,0,sizeof(hdcp2_key));
	memset(lc128_key, 0, sizeof(lc128_key));


	fp = fopen(path, "rb");
	if(fp == NULL)
	{
		printf("fopen fail\n");
		ui->Print("error: the  hdcp2.efuse file  read fail\n");
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	size -= 40;
	if(0 != size % EFUSE_HDCP2_LEN)
	{
		ui->Print("error: the hdcp2.efuse file wrong\n");
		fclose(fp);
		return -1;
	}
	if(size / EFUSE_HDCP2_LEN < hdcpindex){
		ui->Print("error: the hdcp2.efuse file not key to use\n");
		fclose(fp);
		return -1;
	}

	lc128_size = 36;
	fseek(fp, 4, SEEK_SET);
	if((int)fread(lc128_key, 1, lc128_size, fp) != lc128_size)
	{
		fclose(fp);
		ui->Print("error: the hdcp2.efuse file read lc128 fail\n");
		return -1;
	}	

	hdcp2_size = EFUSE_HDCP2_LEN;
	LOGW("sdcard_efuse_write_hdcp() path=%s  size=%d  hdcpindex=%d\n", path, hdcp2_size,hdcpindex);
	index = hdcpindex * EFUSE_HDCP2_LEN + 40;
	fseek(fp, (long)index, SEEK_SET);

	if((int)fread(hdcp2_key, 1, hdcp2_size, fp) != hdcp2_size)
	{
		fclose(fp);
		ui->Print("error: the hdcp2.efuse file read hdcp2 fail\n");
		return -1;
	}
	fclose(fp);
	
	if(verify_hdcp2_key((char *)hdcp2_key, EFUSE_HDCP2_LEN) 
		&& verify_hdcp2_key((char *)lc128_key, 36))  //verify success,hdcp can write
	{
		nand_write_version(PATH_VERSION, NULL);
		ret = nand_write("hdcp2lc128", PATH_KEY_WRITE, (char *)lc128_key, 36);
		if(ret == 0){
			ret = nand_write("hdcp2key", PATH_KEY_WRITE, (char *)hdcp2_key, EFUSE_HDCP2_LEN);
		}
	}
	else
		ret = -1;
	return ret;
}

#endif

#ifdef SDCARD_EFUSE_MAC_ENABLE
static int
efuse_write_mac(const char *path, int type, RecoveryUI* ui)
{
    FILE *fp;
    char *rbuff = NULL, *wbuff = NULL;
    char *line;
    int size, rc, error = 0, offset = 0;
    unsigned char mac[6];

    ui->Print("Finding %s...\n", efuse_title[type]);

    fp = fopen_path(path, "r");
    if (fp == NULL) {
        LOGE("no %s found\n", efuse_title[type]);
        return -1;
    }

    if (efuse_written_check(type, ui)) {
        LOGE("%s written already or something error\n", efuse_title[type]);
        return -1;
    }

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);

    LOGI("efuse_update_mac() path=%s type=%s size=%d\n", path, efuse_title[type], size);

    ui->Print("Reading %s...\n", efuse_title[type]);

    rbuff = (char *)malloc(size + 1);
    wbuff = (char *)malloc(size + 2);
    if (rbuff == NULL || wbuff == NULL) {
        LOGE("system out of resource\n");
        if (rbuff) free(rbuff);
        if (wbuff) free(wbuff);
        check_and_fclose(fp, path);
        return -1;
    }

    fseek(fp, 0, SEEK_SET);
    if ((int)fread(rbuff, 1, size, fp) != size) {
        LOGE("invalid %s\n", efuse_title[type]);
        if (rbuff) free(rbuff);
        if (wbuff) free(wbuff);
        check_and_fclose(fp, path);
        return -1;
    }

    rc = 0;
    rbuff[size] = '\0';
    check_and_fclose(fp, path);

    line = strtok(rbuff, "\n");
    do {
        if (*line == '$' || (strlen(line) != EFUSE_MACLEN && strlen(line) != EFUSE_MACLEN + 1)) {
            offset += strlen(line) + 1 ;
            LOGI("efuse_update_mac() line=\"%s\" SKIP offset=%d\n", line, offset);
            continue;
        }
        for (rc = 0; rc < EFUSE_MACLEN; rc += 3) {
            if (isxdigit(line[rc]) && isxdigit(line[rc + 1]) && (line[rc + 2] == ':' || line[rc + 2] == '\0' || line[rc + 2] == '\r')) {
                mac[rc / 3] = ((isdigit(line[rc]) ? line[rc] - '0' : toupper(line[rc]) - 'A' + 10) << 4) |
                               (isdigit(line[rc + 1]) ? line[rc + 1] - '0' : toupper(line[rc + 1]) - 'A' + 10);
            }
            else
                break;
        }

        if (rc == EFUSE_MACLEN + 1) {
            LOGI("efuse_update_mac() line=\"%s\" MATCH\n", line);

            ui->Print("Writing %s %02x:%02x:%02x:%02x:%02x:%02x\n", efuse_title[type], mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

            if (6 == efuse_write(type, mac, 6, ui)) {
                fp = fopen_path(path, "r");
                if (fp) {
                    fread(rbuff, 1, size, fp);
                    rbuff[size] = '\0';
                    check_and_fclose(fp, path);

                    fp = fopen_path(path, "w+");
                    if (fp) {
                        line = wbuff;
                        memset(wbuff, 0, size + 2);
                        memcpy(line, rbuff, offset);
                        *(line + offset) = '$';
                        memcpy(line + offset + 1, rbuff + offset, size - offset);
                        offset = fwrite(wbuff, 1, size + 1, fp);
                        LOGI("efuse_update_mac() %s wrote rc=%d size=%d\n", path, offset, size + 2);
                        if (offset != size + 1) {
                            error++;
                            LOGE("error updating %s\n", efuse_title[type]);
                        }
                        check_and_fclose(fp, path);
                    }
                }
            }
            else {
                error++;
                LOGE("efuse write error\n");
            }

            break;
        }
        else {
            offset += strlen(line) + 1;
            LOGI("efuse_update_mac() line=\"%s\" INVALID offset=%d\n", line, offset);
        }
    } while((line = strtok(NULL,"\n")));

    if (rc != EFUSE_MACLEN + 1)
        ui->Print("No %s found\n", efuse_title[type]);

    if (rbuff) free(rbuff);
    if (wbuff) free(wbuff);
    return -error;
}
#endif
static int is_normal(char c){
	if((c>='0'&& c <= '9')|| (c >= 'A' &&c <= 'Z')||(c >= 'a'&& c <= 'z')||c == '$'){
		return 1;
	}else{
		return 0;
	}
}

static int get_key_status(RecoveryUI* ui, hwver_mac_addr *mac)
{
	int flag = 0;
  	hwver_use_id usid;
	int wp_enable,fd;
	char key_data[512];
  	fd = open(HWVER_DEV, O_RDONLY);
  	if(fd >= 0){
		//eth mac key check
	  	if (ioctl(fd, HWVER_MAC_READ, mac) >= 0) {
			if(mac->magic == MAGIC){
				flag |= KEY_ETHMAC_FLAG;
		  		ui->Print("Eth mac has been written,mac is %2x:%2x:%2x:%2x:%2x:%2x\n",
					mac->mac[0],mac->mac[1],mac->mac[2],mac->mac[3],mac->mac[4],mac->mac[5]);
			}else{
				ui->Print("Eth mac not written\n");
			}
	  	}
		//usid key check
	  	if (ioctl(fd,HWVER_USE_ID_READ , &usid) >= 0) {
			if(usid.magic == MAGIC){
				flag |= KEY_USID_FLAG;
		  		ui->Print("Usid has been written,usid is %s\n",usid.use_id);
			}else{
				ui->Print("Usid not written\n");
			}
	  	}
		close(fd);
  	}
#ifdef EFUSE_HDCP_ENABLE	
	//hdcp key check
	memset(key_data,0,sizeof(key_data));	
	nand_write_version(PATH_VERSION, NULL);	
	nand_read_hdcp(PATH_KEY_READ, (char *)key_data);	
	if(hdcp_ksv_valid((char *)key_data))	{		
		ui->Print("hdcp key has written before!\n");		
		flag |= KEY_HDCP_FLAG;	
	}else{
		ui->Print("hdcp key not written\n");
	}	
	//hdcp2 key check
	if(is_hdcp2_key_burn())	{		
		ui->Print("hdcp2 key has written before!\n");		
		flag |= KEY_HDCP2_FLAG;	
	}else{
		ui->Print("hdcp2 key not written\n");
	}
#endif


out:
  	if(fd > 0)
		close(fd);	
	return flag;
}

int eeprom_key_write(int key_type, char *key, RecoveryUI* ui)
{
   	char writemac[6];
  	int i, ret = 0;
  	hwver_use_id usid;
  	hwver_mac_addr mac;
  	int wp_enbale;
  	int fd;
	fd = open(HWVER_DEV, O_RDONLY);
	if(fd < 0){
		printf("%s: open %s fail [%s]", __func__,HWVER_DEV, strerror(errno));
		ret = -1;
		goto error;
	}
	wp_enbale = 0;
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		printf("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		ret = -1;
		goto out;
	}
	if(key_type == KEY_TYPE_MAC) {
	  	for (i = 0; i < EFUSE_MACLEN; i += 3) {
			if (isxdigit(key[i]) && isxdigit(key[i + 1]) 
				&& (key[i + 2] == ':' || key[i + 2] == '\0' 
				|| key[i + 2] == '\r'))
		  	{
			  	mac.mac[i / 3] = ((isdigit(key[i]) ? key[i] - '0' : toupper(key[i]) - 'A' + 10) << 4) |
				  (isdigit(key[i + 1]) ? key[i + 1] - '0' : toupper(key[i + 1]) - 'A' + 10);
		  	}else{
			  	break;
		  	}
	  	}
  		mac.magic = MAGIC;
	  	ui->Print("Write mac %02x:%02x:%02x:%02x:%02x:%02x ",
			mac.mac[0], mac.mac[1], mac.mac[2], mac.mac[3], mac.mac[4], mac.mac[5]);
	  	if (ioctl(fd, HWVER_MAC, &mac) < 0) {
		  	ui->Print("failed [%s]\n", strerror(errno));
			ret = -1;
			goto out;
	  	}
	  	ui->Print("ok\n");
	}else if(key_type == KEY_TYPE_USID){
		memset(&usid, 0, sizeof(usid));
		usid.magic = MAGIC;
  		strcpy(usid.use_id,key);
  		ui->Print("Write usid %s ",usid.use_id);
  		if (ioctl(fd,HWVER_USE_ID , &usid) < 0) {
	  		ui->Print("fail [%s]\n", strerror(errno));
			ret = -1;
			goto out;
  		}
  		ui->Print("ok\n");
	}else{
		ret = -1;
	}	
out:
	wp_enbale = 1;
  	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
	  	printf("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		ret = -1;
  	}
error:
  	if(fd > 0)
		close(fd);
  	return ret;
	
}

int eeprom_clean_key(RecoveryUI* ui)
{
  	hwver_use_id usid;
  	hwver_mac_addr mac;
  	int wp_enbale;
  	int fd,ret = 0;
	fd = open(HWVER_DEV, O_RDONLY);
	if(fd < 0){
		printf("%s: open %s fail [%s]", __func__,HWVER_DEV, strerror(errno));
		ret = -1;
		goto error;
	}
	wp_enbale = 0;
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		printf("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		ret = -1;
		goto out;
	}
	memset(&mac, 0, sizeof(mac));
	if (ioctl(fd, HWVER_MAC, &mac) < 0) {
		printf("failed [%s]\n", strerror(errno));
		ret = -1;
		goto out;
	}
	wp_enbale = 0;
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		printf("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		ret = -1;
		goto out;
	}
	memset(&usid, 0, sizeof(usid));
  	if (ioctl(fd,HWVER_USE_ID , &usid) < 0) {
	  	printf("fail [%s]\n", strerror(errno));
		ret = -1;
		goto out;
  	}	
out:
	wp_enbale = 1;
  	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
	  	printf("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		ret = -1;
  	}
error:
  	if(fd > 0)
		close(fd);
	return ret;
}

int nand_clean_keys(RecoveryUI* ui)
{
	int ret = 0;	
	unsigned char hdcp[EFUSE_HDCP_LEN];
	unsigned char hdcp2_key[EFUSE_HDCP2_LEN];
	unsigned char lc128_key[36];

	memset(hdcp, 0, sizeof(hdcp));
	memset(hdcp2_key,0,sizeof(hdcp2_key));
	memset(lc128_key, 0, sizeof(lc128_key));
	nand_write_version(PATH_VERSION, NULL);
	ret = nand_write_hdcp(PATH_KEY_WRITE, (char *)hdcp);
	if(ret >= 0){
		nand_write_version(PATH_VERSION, NULL);
		ret = nand_write("hdcp2lc128", PATH_KEY_WRITE, (char *)lc128_key, 36);
		if(ret >= 0){
			ret = nand_write("hdcp2key", PATH_KEY_WRITE, (char *)hdcp2_key, EFUSE_HDCP2_LEN);
		}
	}
	return ret;
}

static int  hwver_write_mac_use_id(const char * pusid,const char * pmac,RecoveryUI* ui)
{
   char writemac[6];
  int i;
  hwver_use_id usid;
  hwver_mac_addr mac;
  int wp_enbale;
  int fd;

 //init mac
  for (i = 0; i < EFUSE_MACLEN; i += 3) {
	  if (isxdigit(pmac[i]) && isxdigit(pmac[i + 1]) && (pmac[i + 2] == ':' || pmac[i + 2] == '\0' || pmac[i + 2] == '\r'))
	  {
		  writemac[i / 3] = ((isdigit(pmac[i]) ? pmac[i] - '0' : toupper(pmac[i]) - 'A' + 10) << 4) |
			  (isdigit(pmac[i + 1]) ? pmac[i + 1] - '0' : toupper(pmac[i + 1]) - 'A' + 10);
	  }
	  else
		  break;
  }
  i = 0;
  for(i = 0;i < 6;i++){
	  mac.mac[i]=writemac[i];	  
  }
  mac.magic = MAGIC;
  //init usid
  usid.magic = MAGIC;
  memcpy(usid.use_id,pusid,16);

  fd = open(HWVER_DEV, O_RDONLY);
  if(fd < 0){
	  ui->Print("%s: open %s fail [%s]", __func__,HWVER_DEV, strerror(errno));
	  goto out;
  }
  wp_enbale = 0;
  if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
	  ui->Print("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
	  goto out;
  }
  ui->Print("Write mac %02x:%02x:%02x:%02x:%02x:%02x\n",writemac[0], writemac[1], writemac[2], writemac[3], writemac[4], writemac[5]);
  if (ioctl(fd, HWVER_MAC, &mac) < 0) {
	  ui->Print("%s: ioctl write HWVER_MAC fail [%s]", __func__, strerror(errno));
	  goto out;
  }
  ui->Print("Write mac ok\n");

  ui->Print("Write use id %s\n",pusid);
  if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
	  ui->Print("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
	  goto out;
  }
  if (ioctl(fd,HWVER_USE_ID , &usid) < 0) {
	  ui->Print("%s: ioctl write HWVER_USE_ID fail [%s]", __func__, strerror(errno));
	  goto out;
  }
  ui->Print("Write use id ok");

  close(fd);
  return 0;
out:
  if(fd > 0)close(fd);
  return -1;
}

#ifdef SDCARD_EEPROM_MAC_USE_ID_ENABLE
int addEfuseInfo(struct t_key_info **ptr, int index, char *value)
{
	struct t_key_info *p = *ptr;
	struct t_key_info *info = (struct t_key_info *)malloc(sizeof(struct t_key_info *));
	
	if(info == NULL){
		printf("malloc efuse info failed\n");
		return -1;
	}
	info->index = index;
	info->key = (char *)strdup(value);
	info->next = NULL;
	if(p == NULL){
		*ptr = info;
	}else{
		while(p->next){
			p = p->next;
		}	
		p->next = info;
	}
	return 0;
}

int get_keyinfo(struct t_key_info **key_info, char *ptr)
{
	char *p;
	int index = 0, ret = 0;
    p = strtok(ptr,"\t\r\n ");
	if(*p == '$')
		p ++;
	index = 0;
    while(p){
    	//printf("%s\n",p);
		ret = addEfuseInfo(key_info, index, p);
		index ++;
    	p=strtok(NULL,"\t\r\n ");
    }
	return ret;
}

int free_keyinfo(struct t_key_info *info)
{
	struct t_key_info *next;
	while(info){
		next = info->next;
		if(info->key)
			free(info->key);
		free(info);
		info = next;
	}
	return 0;
}

int check_mac(struct t_key_info *key_info, char *mac)
{
   	char writemac[6];
	char *pmac;
   	int i;
	struct t_key_info *p = key_info;
	if(p == NULL)
		return 0;
	if(key_info->index != 0)
		return 0;
	pmac = key_info->key;
 	for (i = 0; i < EFUSE_MACLEN; i += 3) {
		if (isxdigit(pmac[i]) && isxdigit(pmac[i + 1]) && (pmac[i + 2] == ':' || pmac[i + 2] == '\0' || pmac[i + 2] == '\r'))
		{
			writemac[i / 3] = ((isdigit(pmac[i]) ? pmac[i] - '0' : toupper(pmac[i]) - 'A' + 10) << 4) |
				  (isdigit(pmac[i + 1]) ? pmac[i + 1] - '0' : toupper(pmac[i + 1]) - 'A' + 10);
		}
		else
			break;
 	}
	for(i = 0; i < 6; i ++) {
		if(writemac[i] != mac[i])
			return 0;
	}
	return 1;
}

void printf_key_info(struct t_key_info *info)
{
	struct t_key_info *p = info;
	printf("printf_key_info\n");
	if(p == NULL)
		printf("printf_key_info is NULL\n");
	while(p){
		printf("efuse info index = %d,key = %s\n",p->index,p->key);
		p = p->next;
	}
}

int burn_key_info(struct t_key_info *info, int flag, RecoveryUI* ui)
{
	int result_flag = 0;
	char *mac = NULL, *usid = NULL, *hdcp_index = NULL;
	struct t_key_info *p = info;
	while(p){
		switch(p->index){
			case 0:
				mac = p->key;
				break;
			case 1:
				usid = p->key;
				break;
			case 2:
				hdcp_index = p->key;
				break;
		};
		p = p->next;
	}
	printf("burn_key_info mac = %s, usid = %s, hdcp_index = %s, flag = 0x%x\n",mac,usid,hdcp_index,flag);
	
	if(((flag & KEY_ETHMAC_FLAG) == 0) && mac){
		if(eeprom_key_write(KEY_TYPE_MAC, mac, ui) >= 0){
			result_flag |= KEY_ETHMAC_FLAG;
		}
	}
	if(((flag & KEY_USID_FLAG) == 0) && usid){
		if(eeprom_key_write(KEY_TYPE_USID, usid, ui) >= 0){
			result_flag |= KEY_USID_FLAG;
		}
	}
	if(((flag & KEY_HDCP_FLAG) == 0) && hdcp_index){
		int index = atoi(hdcp_index);
		if(write_hdcp(index-1,ui) < 0){
			ui->Print("hdcp key written fail ,the reason as above\n");
		}else{
			ui->Print("hdcp key written ok! index = %d\n",index);
			result_flag |= KEY_HDCP_FLAG;
		}
	}
	if(((flag & KEY_HDCP2_FLAG) == 0) && hdcp_index){
		int index = atoi(hdcp_index);
		if(write_hdcp2(index-1,ui) < 0){
			ui->Print("hdcp2 key written fail ,the reason as above\n");
		}else{
			ui->Print("hdcp2 key written ok! index = %d\n",index);
			result_flag |= KEY_HDCP2_FLAG;
		}
	}
	return result_flag;
}

static int
eeprom_burn_keys(const char *path, int type, RecoveryUI* ui) 
{
	FILE *fp;
	char line_buf[LINE_BUF_SIZE],op_buf[LINE_BUF_SIZE],pmac[50],pusid[50];
	char * tmp,*p;
	char *rbuff = NULL, *line = NULL;
	int  k,offset,size,wr_size,ret = 0,flag = 0, ret_flag = 0; 
    size_t len = 0;
    ssize_t read;
	hwver_mac_addr mac;
	struct t_key_info *key_info = NULL;
	
	ui->Print("Start burning keys...\n");
	flag = get_key_status(ui, &mac);
	flag &= KEY_MASK;
	ensure_path_unmounted(path);
	fp = fopen_path(path, "r");
	if (fp == NULL) {
		ui->Print("error: not mac and use id found,%s \n",strerror(errno));
		ret = -1;
		goto out;
	}
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);			
	rbuff = (char*)malloc(size + 2);
	if (rbuff == NULL) {
		printf("cannot malloc read buffer\n");
		ret = -1;
		goto out;
	}
	printf("file size = %d\n",size);
	offset = 0;
    while ((read = getline(&line, &len, fp)) > 0) {
		memcpy(rbuff+offset, line, read);
    	//printf("Retrieved line of length %ld :\n", read);
    	printf("%s, size = %d, len = %d\n", line,read,len);
    	if(line[0] == '$'){
			offset += read;
			if(flag & KEY_ETHMAC_FLAG == 0){
				continue;
			}else{
				if(get_keyinfo(&key_info, line) < 0){
					free_keyinfo(key_info);
					key_info = NULL;
					continue;
				}else if(check_mac(key_info, mac.mac)){
					break;
				}else{
					free_keyinfo(key_info);
					key_info = NULL;
					continue;
				}
			}
    	}else if(flag & KEY_ETHMAC_FLAG){
    		ui->Print("ethernet mac has been written,ethmac.efuse file no include this mac %2x:%2x:%2x:%2x:%2x:%2x\n",
				  mac.mac[0],mac.mac[1],mac.mac[2],mac.mac[3],mac.mac[4],mac.mac[5]);
    		break;
    	}
		if(get_keyinfo(&key_info, line) >= 0){
			break;
		}
		free_keyinfo(key_info);
		key_info = NULL;
    }
	if(line)
		free(line);
	//printf_key_info(key_info);

	if(key_info == NULL){
		ret = -1;
		if((flag & KEY_ETHMAC_FLAG) == 0){
			ui->Print("Error,Ethmac in all the key has finished burning,no new key can burn! Please check ethmac file!\n");
		}
		goto out;
	}
	ret_flag = burn_key_info(key_info, flag, ui);
	if(ret_flag > 0){
		ui->Print("New key ");
		if(ret_flag & KEY_ETHMAC_FLAG){
			ui->Print("mac ");
		}
		if(ret_flag & KEY_USID_FLAG){
			ui->Print("usid ");
		}
		if(ret_flag & KEY_HDCP_FLAG){
			ui->Print("hdcp ");
		}
		if(ret_flag & KEY_HDCP2_FLAG){
			ui->Print("hdcp2 ");
		}
		ui->Print("is written!\n");
	}else {
		ret = -1;
		ui->Print("Not any new key has written!\n");
		goto out;
	}
	if(((flag & KEY_ETHMAC_FLAG) == 0) 
		&& ( ret_flag & KEY_ETHMAC_FLAG)){
		char *p = rbuff + offset + 1;
		fseek(fp, offset, SEEK_SET);	
		rbuff[offset] = '$';		
		if ((int)fread(p, 1, size-offset, fp) != (size-offset)) {			
			ret = -1;
			printf("get nother file failed\n");
			goto out;
		}
		rbuff[size+1] = '\0';
		printf("write buff is %s\n", rbuff);
		check_and_fclose(fp, path);
		fp = NULL;
		fp = fopen_path(path, "w+");					
		if (fp) {
			offset = fwrite(rbuff, 1, size + 1, fp);						
			LOGI("save efuse file %s wrote rc=%d size=%d\n", path, offset, size + 2);						
			if (offset != size + 1) {							
				ret = -1;							
				LOGE("error updating %s\n", efuse_title[type]);
				goto out;
			}						
			check_and_fclose(fp, path);
			fp = NULL;
			sync();					
		}
	}
out:
	if(fp != NULL)
		check_and_fclose(fp, path);
	sync();
	if (rbuff) {
		free(rbuff);
		rbuff = NULL;
	}
	return ret;
}

static int  eeprom_clean_keys(RecoveryUI* ui, Device* device)
{
	FILE *fp;
	int ret = 0;
    static const char** title_headers = NULL;

    if (title_headers == NULL) {
    	const char* headers[] = { "Confirm clear all keys? do you want clean?",
                                      "  THIS CAN NOT BE UNDONE.",
                                      "",
                                      NULL };
        title_headers = prepend_title((const char**)headers);
	}

    const char* items[] = { " No",
                            " No",
                            " No",
                            " No",
                            " No",
                            " No",
                            " No",
                            " Yes -- clean all keys",   // [7]
                            " No",
                            " No",
                            " No",
                            NULL };

	int chosen_item = get_menu_selection(title_headers, items, 1, 0, device);
	if (chosen_item != 7) {
		return -1;
	}

    ui->Print("\n-- Claen all keys...\n");
	ret = eeprom_clean_key(ui);
	if(ret >= 0){
		ret = nand_clean_keys(ui);
	}
    ui->Print("Claen all keys %s.\n", ret < 0 ? "failed" : "complete");
	return ret;	
}
#endif

#ifdef SDCARD_EEPROM_HARDVERSION_ENABLE
static void show_hardversion(RwHwVersion ioctl_hwver,RecoveryUI* ui)
{
	int ver;
  //show to ui
	if(ioctl_hwver.hwver.dtbversion < DTB_DOGNLE_VER1){
		ver = (char)(ioctl_hwver.hwver.dtbversion - DTB_NORMAL_VER1 + 1);
		ui->Print("dtbname: normal_ver%d\n",ver);
	}else if((ioctl_hwver.hwver.dtbversion >= DTB_DOGNLE_VER1)
			&& (ioctl_hwver.hwver.dtbversion < DTB_HDMIIN_VER1)){
		ver = (char)(ioctl_hwver.hwver.dtbversion - DTB_DOGNLE_VER1 + 1);
		ui->Print("dtbname: dongle_ver%d\n",ver);
	}else if(ioctl_hwver.hwver.dtbversion >= DTB_HDMIIN_VER1){
		ver = (char)(ioctl_hwver.hwver.dtbversion - DTB_HDMIIN_VER1 + 1);
		ui->Print("dtbname: hdmiin_ver%d\n",ver);
	}  

	if(ioctl_hwver.hwver.wifiname<WIFI_NAME_MAX){
		ui->Print("wifi: %s\n",wifi_name[ioctl_hwver.hwver.wifiname]);
	}
	
}
static int eeprom_read_hardversion(RecoveryUI* ui)
{
	int fd;
	int wp_enbale;
	RwHwVersion ioctl_hwver;

	//write hwver
	fd = open(HWVER_DEV, O_RDWR);
	if(fd < 0){
		ui->Print("%s: open %s fail [%s]\n", __func__,HWVER_DEV, strerror(errno));
		goto out;
	}
	wp_enbale = 0;
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		ui->Print("%s: ioctl write WP_ENABLE fail [%s]\n", __func__, strerror(errno));
		goto out;
	}

	ioctl_hwver.is_wr = 0;
	if (ioctl(fd, HWVER_HARDWARE_VERSION, &ioctl_hwver) < 0) {
		ui->Print("%s: ioctl read HWVER_HARDWARE_VERSION fail [%s]\n", __func__, strerror(errno));
		goto out;
	}
	if(ioctl_hwver.hwver.magic != MAGIC){
		ui->Print("hwver has not written before\n");
		goto out;
	}
	SHOW_HWVER(ioctl_hwver.hwver,"hwer read from eeprom:\n");
	show_hardversion(ioctl_hwver,ui);

	if(fd > 0)close(fd);
	return 0;

out:
	if(fd > 0)close(fd);
	return -1;
}

static  int write_hardversion(m_HwVersion hwver,RecoveryUI* ui)
{
	int fd;
	int i;
	int wp_enbale;
	char * sd_hwver,* eeprom_hwver;
	RwHwVersion ioctl_hwver; 

	//write hwver
	fd = open(HWVER_DEV, O_RDONLY);
	if(fd < 0){
		ui->Print("%s: open %s fail [%s]", __func__,HWVER_DEV, strerror(errno));
		goto out;
	}
	wp_enbale = 0;
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		ui->Print("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		goto out;
	}

    ioctl_hwver.hwver = hwver;
	ioctl_hwver.is_wr = 1;
	if (ioctl(fd, HWVER_HARDWARE_VERSION, &ioctl_hwver) < 0) {
		ui->Print("%s: ioctl write HWVER_HARDWARE_VERSION fail [%s]", __func__, strerror(errno));
		goto out;
	}
	//test wr data match?
	if (ioctl(fd, HWVER_WP_ENABLE, &wp_enbale) < 0) {
		ui->Print("%s: ioctl write WP_ENABLE fail [%s]", __func__, strerror(errno));
		goto out;
	}

    ioctl_hwver.hwver = hwver;
	ioctl_hwver.is_wr = 0;
	if (ioctl(fd, HWVER_HARDWARE_VERSION, &ioctl_hwver) < 0) {
		ui->Print("%s: ioctl write HWVER_HARDWARE_VERSION fail [%s]", __func__, strerror(errno));
		goto out;
	}

    sd_hwver = (char *)&hwver;  
	eeprom_hwver =(char *)&ioctl_hwver.hwver;
	  
	 for(i=0;i<sizeof(m_HwVersion);i++){
	   if(sd_hwver[i]!=eeprom_hwver[i]){
	     ui->Print("write hwver fail %d byte not match\n",i);	   
		 goto out;
	  }	 
	}
	if(fd > 0)close(fd);
	return 0;

out:
	if(fd > 0)close(fd);
	return -1;
}

static int eeprom_write_hardversion(const char *path,RecoveryUI* ui)
{
	FILE *fp;
	int size;
	m_HwVersion  hwver;

	ensure_path_unmounted(path);

	ui->Print("Finding mac and use id...\n");

	fp = fopen_path(path, "r");
	if (fp == NULL) {
		ui->Print("error: not mac and use id found,%s \n",strerror(errno));
		return -1;
	}

	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
    if(size != HARDWARE_SIZE){
	  ui->Print("error:hwver not right\n");	
	  goto out;
	}	

	fseek(fp, 0, SEEK_SET);
	if ((int)fread((void *)&hwver, 1, size, fp) != size) {
		ui->Print("error:fread %s\n",strerror(errno));
		goto out;
	}
	if(hwver.magic != MAGIC){
		ui->Print("hwver magic not right\n");	
		goto out;
	}
	if(write_hardversion(hwver,ui)<0){
		goto out;
	}

	check_and_fclose(fp, path);
	return 0;

out:
	check_and_fclose(fp, path);
	return -1;
}
#endif

#ifdef EFUSE_LICENCE_ENABLE

static int
efuse_audio_license_decode(char *raw, unsigned char *license)
{
    int i;
    char * curr = raw;

    LOGE("efuse_audio_license_decode() raw=%s\n", raw);
    if (!curr)
        return -1;

    for (*license = 0, i = EFUSE_BYTES; i > 0; i--, curr++) {
        if (*curr == '1')
            *license |= 1 << (i - 1);
    }   
    
    LOGE("efuse_audio_license_decode() license=%x\n", *license);
    return 0;
}

/**
 * There are 4 bytes for licence,now we use byte 1--bit[1:0] for cntl 0-ac3,1-dts.
 */
int
efuse_write_audio_license(char *path, , RecoveryUI* ui)
{
    int fd;
    FILE *fp;
    unsigned char license = 0;
    char raw[32];

    ui->Print("Finding %s...\n", efuse_title[EFUSE_LICENCE]);
    fp = fopen_path(path, "r");
    if (fp == NULL) {
        LOGE("no %s found\n", efuse_title[EFUSE_LICENCE]);
        return -1;
    }

    ui->Print("Reading %s...\n", efuse_title[EFUSE_LICENCE]);
    fgets(raw, sizeof(raw), fp);
    if (strlen(raw) < EFUSE_BYTES) {
        LOGE("invalid %s\n", efuse_title[EFUSE_LICENCE]);
        check_and_fclose(fp, path);
        return -1;
    }

    check_and_fclose(fp, path);
    if (efuse_audio_license_decode(raw, &license)) {
        LOGE("invalid %s\n", efuse_title[EFUSE_LICENCE]);
        check_and_fclose(fp, path);
        return -1;
    }

    ui->Print("Writing %s...\n", efuse_title[EFUSE_LICENCE]);
    fd = efuse_opendev();
    if (fd < 0)
        return -1;

    if (lseek(fd, 0, SEEK_SET) == 0) {
        if (write(fd, &license, sizeof(license)) == 1) {
            if ((license & 0x3) > 0)
                ui->Print("Audio license enabled\n");
            else
                ui->Print("Audio license wrote\n");
        }
        else {
            LOGE("efuse write error\n");
            efuse_closedev(fd);
            return -1;
        }
    }

    efuse_closedev(fd);
    return 0;
}
#endif /*EFUSE_LICENCE_ENABLE */

/**
 *  Recovery efuse programming UI, current support efuse items:
 *  
 *    Audio license
 *    Ethernet MAC address
 *    Bluetooth MAC address
 */
int
recovery_efuse(int interactive, const char* args, Device * device)
{
    const char* menu[] = { "Choose an efuse item to program:",
                           "",
                           NULL };
    
    char prop[PROPERTY_VALUE_MAX];
    int result = 0;
    int chosen_item = 0;
    int efuse_item_index = 0;
    const char **headers = NULL;
    RecoveryUI * ui = device->GetUI();

    if (interactive < 0) {
        headers = prepend_title((const char**)menu);
        chosen_item = get_menu_selection(headers, (char **)efuse_items, 1, chosen_item, device);
        efuse_item_index = efuse_item_id[chosen_item];
    }
    else
        efuse_item_index = interactive;

    if (efuse_item_index > EFUSE_NONE && efuse_item_index < EFUSE_TYPE_MAX)
        ui->Print("\n-- Program %s...\n", efuse_title[efuse_item_index]);

    switch (efuse_item_index) {
        case EFUSE_VERSION:
             result = efuse_write_version((char *)args, ui);
             break;
#ifdef EFUSE_LICENCE_ENABLE
        case EFUSE_LICENCE:
            result = efuse_write_audio_license((char *)SDCARD_AUDIO_LICENSE, ui);
            break;
#endif /* EFUSE_LICENCE_ENABLE */
#ifdef SDCARD_EFUSE_MAC_ENABLE
        case EFUSE_MAC:
            result = efuse_write_mac((char *)SDCARD_ETHERNET_MAC, efuse_item_index, ui);
            break;
        case EFUSE_MAC_BT:
            result = efuse_write_mac((char *)SDCARD_BLUETOOTH_MAC, efuse_item_index, ui);
            break;
#endif 
    }

    if (efuse_item_index > EFUSE_NONE && efuse_item_index < EFUSE_TYPE_MAX) {
        if (result)
            ui->Print("Failed to write %s\n", efuse_title[efuse_item_index]);
        else
            ui->Print("\nWrite %s complete\n", efuse_title[efuse_item_index]);
    }

    if (headers) free(headers);
    return result;
}

int recovery_operate_eeprom(int interactive, const char* args, Device * device)
{
	const char* menu[] = { "Choose burn or clean mac usid hdcp hdcp2 key to program:",
#ifdef  SDCARD_EEPROM_HARDVERSION_ENABLE
		"burn or display hardware version to program:",
#endif		
		"",
		NULL };

	char prop[PROPERTY_VALUE_MAX];
	int result = 0;
	int chosen_item = 0;
	int eeprom_item_index = 0;
	const char **headers = NULL;
	RecoveryUI * ui = device->GetUI();

	if (interactive < 0) {
		headers = prepend_title((const char**)menu);
		chosen_item = get_menu_selection(headers, (char **)eeprom_items, 1, chosen_item, device);
		eeprom_item_index = eeprom_item_id[chosen_item];
	}
	else
		eeprom_item_index = interactive;

	if (eeprom_item_index > EEPROM_NONE && eeprom_item_index < EEPROM_TYPE_MAX)
		ui->Print("\n-- Program %s...\n", eeprom_title[eeprom_item_index]);

	switch (eeprom_item_index) {
#ifdef SDCARD_EEPROM_MAC_USE_ID_ENABLE
		case BURN_KEY:
			result = eeprom_burn_keys((char *)SDCARD_ETHERNET_MAC, eeprom_item_index, ui);
			break;
		case CLEAN_KEY:
			result = eeprom_clean_keys(ui, device);
			break;
#endif
#ifdef  SDCARD_EEPROM_HARDVERSION_ENABLE
		case BURN_HARDVERSION:
			result = eeprom_write_hardversion((char *)SDCARD_HARDVERSION,ui);
			break;			
		case GET_HARDVERSION:
			result = eeprom_read_hardversion(ui);
			break;
#endif
		default :
			break;

	}

	if (eeprom_item_index > EEPROM_NONE && eeprom_item_index < EEPROM_TYPE_MAX) {
		if (result)
			ui->Print("Failed to %s\n", eeprom_title[eeprom_item_index]);
		else
			ui->Print("%s complete\n", eeprom_title[eeprom_item_index]);
	}

	if (headers) free(headers);
	return result;
}
