# Copyright (C) 2007 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

ifeq ($(TARGET_RECOVERY_HAS_EFUSE),)
TARGET_RECOVERY_HAS_EFUSE := true
endif

ifeq ($(TARGET_RECOVERY_HAS_EEPROM),)
TARGET_RECOVERY_HAS_EEPROM := true
endif

ifeq ($(TARGET_SDCARD_EFUSE_MAC_ENABLE),)
TARGET_SDCARD_EFUSE_MAC_ENABLE := true
endif

ifeq ($(TARGET_SDCARD_EEPROM_MAC_USE_ID_ENABLE),)
TARGET_SDCARD_EEPROM_MAC_USE_ID_ENABLE := true
endif

ifeq ($(TARGET_SDCARD_EEPROM_HARDVERSION_ENABLE),)
TARGET_SDCARD_EEPROM_HARDVERSION_ENABLE := true
endif
commands_recovery_local_path := $(LOCAL_PATH)

LOCAL_SRC_FILES := \
    recovery.cpp \
    bootloader.cpp \
    install.cpp \
    roots.cpp \
    ui.cpp \
    screen_ui.cpp \
    verifier.cpp \
    adb_install.cpp \
    cmd_excute.cpp \
    efuse.cpp
    
LOCAL_C_INCLUDES += external/fw_env/ \
    system/core/fs_mgr/include

ifeq ($(strip $(UBI_SUPPORT)),true)
LOCAL_SRC_FILES += \
       ubi/ubiutils-common.c \
       ubi/libubi.c
LOCAL_C_INCLUDES += \
       external/mtd-utils/include/ \
       external/mtd-utils/ubi-utils/include
LOCAL_CFLAGS += -DUBI_SUPPORT=1
endif

LOCAL_MODULE := recovery

LOCAL_FORCE_STATIC_EXECUTABLE := true

RECOVERY_API_VERSION := 3
RECOVERY_FSTAB_VERSION := 2
LOCAL_CFLAGS += -DRECOVERY_API_VERSION=$(RECOVERY_API_VERSION)

LOCAL_STATIC_LIBRARIES := \
    libext4_utils_static \
    libsparse_static \
    libminzip \
    libz \
    libmtdutils \
    libmincrypt \
    libminadbd \
    libminui \
    libpixelflinger_static \
    libpng \
    libfs_mgr \
    libcutils \
    liblog \
    libselinux \
    libstdc++ \
    libm \
    libc

ifeq ($(TARGET_USERIMAGES_USE_EXT4), true)
    LOCAL_CFLAGS += -DUSE_EXT4
    LOCAL_C_INCLUDES += system/extras/ext4_utils
    LOCAL_STATIC_LIBRARIES += libext4_utils_static libz
endif

ifeq ($(USB_BURN), true)
    LOCAL_CFLAGS += -DUSB_BURN
endif
ifeq ($(DEFENV_IN_FACTORY_RESET), true)
    LOCAL_CFLAGS += -DDEFENV_IN_FACTORY_RESET
endif

# This binary is in the recovery ramdisk, which is otherwise a copy of root.
# It gets copied there in config/Makefile.  LOCAL_MODULE_TAGS suppresses
# a (redundant) copy of the binary in /system/bin for user builds.
# TODO: Build the ramdisk image in a more principled way.
LOCAL_MODULE_TAGS := eng

ifeq ($(TARGET_RECOVERY_UI_LIB),)
  LOCAL_SRC_FILES += default_device.cpp
else
  LOCAL_STATIC_LIBRARIES += $(TARGET_RECOVERY_UI_LIB)
endif

LOCAL_STATIC_LIBRARIES += libfw_env

ifeq ($(TARGET_RECOVERY_HAS_MEDIA),true)
LOCAL_CFLAGS += -DRECOVERY_HAS_MEDIA
endif # TARGET_RECOVERY_HAS_MEDIA == true

ifeq ($(TARGET_RECOVERY_HAS_PARAM),true)
LOCAL_CFLAGS += -DRECOVERY_HAS_PARAM
endif # TARGET_RECOVERY_HAS_PARAM == true

ifeq ($(TARGET_RECOVERY_MEDIA_LABEL),)
LOCAL_CFLAGS += -DRECOVERY_MEDIA_LABEL="\"android\""
else
LOCAL_CFLAGS += -DRECOVERY_MEDIA_LABEL=$(TARGET_RECOVERY_MEDIA_LABEL)
endif

ifeq ($(TARGET_RECOVERY_HAS_EFUSE),true)
LOCAL_CFLAGS += -DRECOVERY_HAS_EFUSE
endif

ifeq ($(TARGET_RECOVERY_HAS_EEPROM),true)
LOCAL_CFLAGS += -DRECOVERY_HAS_EEPROM
endif

ifeq ($(TARGET_SDCARD_EFUSE_MAC_ENABLE),true)
LOCAL_CFLAGS += -DSDCARD_EFUSE_MAC_ENABLE
endif

ifeq ($(TARGET_SDCARD_EEPROM_MAC_USE_ID_ENABLE),true)
LOCAL_CFLAGS += -DSDCARD_EEPROM_MAC_USE_ID_ENABLE
endif

ifeq ($(TARGET_SDCARD_EEPROM_HARDVERSION_ENABLE),true)
LOCAL_CFLAGS += -DSDCARD_EEPROM_HARDVERSION_ENABLE
endif

ifeq ($(TARGET_RECOVERY_WRITE_KEY),true)
LOCAL_CFLAGS += -DRECOVERY_WRITE_KEY
endif

ifeq ($(TARGET_RECOVERY_WIPE_BOOT_BEFORE_UPGRADE),true)
LOCAL_CFLAGS += -DRECOVERY_WIPE_BOOT_BEFORE_UPGRADE
endif
##############################
ifeq ($(TARGET_BOARD_PLATFORM), meson8)
LOCAL_CFLAGS += -DMESON8
endif
ifeq ($(TARGET_BOARD_PLATFORM), meson6)
LOCAL_CFLAGS += -DMESON6
endif
ifeq ($(TARGET_BOARD_PLATFORM), meson3)
LOCAL_CFLAGS += -DMESON3
endif

ifeq ($(TARGET_RECOVERY_HAS_FACTORY_TEST),true)
LOCAL_CFLAGS += -DRECOVERY_HAS_FACTORY_TEST
endif

LOCAL_CFLAGS += -DEFUSE_HDCP_ENABLE

LOCAL_C_INCLUDES += system/extras/ext4_utils

include $(BUILD_EXECUTABLE)



include $(CLEAR_VARS)
LOCAL_MODULE := verifier_test
LOCAL_FORCE_STATIC_EXECUTABLE := true
LOCAL_MODULE_TAGS := tests
LOCAL_SRC_FILES := \
    verifier_test.cpp \
    verifier.cpp \
    ui.cpp
LOCAL_STATIC_LIBRARIES := \
    libmincrypt \
    libminui \
    libcutils \
    libstdc++ \
    libc
include $(BUILD_EXECUTABLE)


include $(LOCAL_PATH)/minui/Android.mk \
    $(LOCAL_PATH)/minelf/Android.mk \
    $(LOCAL_PATH)/minzip/Android.mk \
    $(LOCAL_PATH)/minadbd/Android.mk \
    $(LOCAL_PATH)/mtdutils/Android.mk \
    $(LOCAL_PATH)/tools/Android.mk \
    $(LOCAL_PATH)/edify/Android.mk \
    $(LOCAL_PATH)/updater/Android.mk \
    $(LOCAL_PATH)/applypatch/Android.mk
