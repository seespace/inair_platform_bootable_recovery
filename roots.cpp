/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <stdlib.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>

#include <fs_mgr.h>
#include "mtdutils/mtdutils.h"
#include "mtdutils/mounts.h"
#include "roots.h"
#include "common.h"
#include "make_ext4fs.h"

#if UBI_SUPPORT
#include "libubi.h"
#define DEFAULT_CTRL_DEV "/dev/ubi_ctrl"
static int format_ubifs_volume(const char* location);
#endif

#include "cutils/properties.h"
#include "cmd_excute.h"

//FIXME device2 does not exist
#define device2 blk_device
#define DEFAULT_CTRL_DEV "/dev/ubi_ctrl"
#define NUM_OF_PARTITION_TO_ENUM       4 
static Volume* device_volumes = NULL;
static struct fstab *fstab = NULL;


extern struct selabel_handle *sehandle;

static int parse_options(char* options, Volume* volume) {
    char* option;
    while ((option = strtok(options, ","))) {
        options = NULL;

        if (strncmp(option, "length=", 7) == 0) {
            volume->length = strtoll(option+7, NULL, 10);
        } else {
            LOGE("bad option \"%s\"\n", option);
            return -1;
        }
    }
    return 0;
}

void load_volume_table() {
    int i;
    int ret;
    int alloc = 2;
    device_volumes = (Volume*)malloc(alloc * sizeof(Volume));

    fstab = fs_mgr_read_fstab("/etc/recovery.fstab");
    if (!fstab) {
        LOGE("failed to read /etc/recovery.fstab\n");
        return;
    }

    ret = fs_mgr_add_entry(fstab, "/tmp", "ramdisk", "ramdisk", 0);
    if (ret < 0 ) {
        LOGE("failed to add /tmp entry to fstab\n");
        fs_mgr_free_fstab(fstab);
        fstab = NULL;
        return;
    }

    printf("recovery filesystem table\n");
    printf("=========================\n");
    for (i = 0; i < fstab->num_entries; ++i) {
        Volume* v = &fstab->recs[i];
        printf("  %d %s %s %s %lld\n", i, v->mount_point, v->fs_type,
               v->blk_device, v->length);
    }
    printf("\n");
}

Volume* volume_for_path(const char* path) {
    return fs_mgr_get_entry_for_mount_point(fstab, path);
}

int auto_mount_fs(char *device_name, Volume *vol){

	if(!strcmp(vol->fs_type, "auto")){
		if (!mount(device_name, vol->mount_point, "vfat",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME, "")) {
			goto auto_mounted;
		}

		// if sdcard mount failed, then read-only mount again
		if(strstr(vol->mount_point, "sdcard")) {
			LOGW("mount %s on %s failed (%s),now try read-only to mount again ...\n", device_name, vol->mount_point, strerror(errno));
			if (!mount(device_name, vol->mount_point, "vfat",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME | MS_RDONLY, 0)) {
			    LOGW("read-only mount %s on %s successful !!!\n", device_name, vol->mount_point);
			    goto auto_mounted;
			}else {
			    LOGW("read-only mount %s on %s failed (%s) !!!\n", device_name, vol->mount_point, strerror(errno));
			}
		}

		if (!mount(device_name, vol->mount_point, "ntfs",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME, "")) {
			goto auto_mounted;
		}

		// if sdcard mount failed, then read-only mount again
		if(strstr(vol->mount_point, "sdcard")) {
			LOGW("mount %s on %s failed (%s),now try read-only to mount again ...\n", device_name, vol->mount_point, strerror(errno));
			if (!mount(device_name, vol->mount_point, "ntfs",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME | MS_RDONLY, 0)) {
			    LOGW("read-only mount %s on %s successful !!!\n", device_name, vol->mount_point);
			    goto auto_mounted;
			}else {
			    LOGW("read-only mount %s on %s failed (%s) !!!\n", device_name, vol->mount_point, strerror(errno));
			}
		}

		if (!mount(device_name, vol->mount_point, "exfat",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME, "")) {
			goto auto_mounted;
		}

		// if sdcard mount failed, then read-only mount again
		if(strstr(vol->mount_point, "sdcard")) {
			LOGW("mount %s on %s failed (%s),now try read-only to mount again ...\n", device_name, vol->mount_point, strerror(errno));
			if (!mount(device_name, vol->mount_point, "exfat",
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME | MS_RDONLY, 0)) {
			    LOGW("read-only mount %s on %s successful !!!\n", device_name, vol->mount_point);
			    goto auto_mounted;
			}else {
			    LOGW("read-only mount %s on %s failed (%s) !!!\n", device_name, vol->mount_point, strerror(errno));
			}
		}
	}
	else{
		if(!mount(device_name, vol->mount_point, vol->fs_type,
			   MS_NOATIME | MS_NODEV | MS_NODIRATIME, ""))
		goto auto_mounted;

		// if sdcard mount failed, then read-only mount again
		if(strstr(vol->mount_point, "sdcard")) {
			LOGW("mount %s on %s failed (%s),now try read-only to mount again ...\n", device_name, vol->mount_point, strerror(errno));
			if (!mount(device_name, vol->mount_point, vol->fs_type,
				   MS_NOATIME | MS_NODEV | MS_NODIRATIME | MS_RDONLY, 0)) {
			    LOGW("read-only mount %s on %s successful !!!\n", device_name, vol->mount_point);
			    goto auto_mounted;
			}else {
			    LOGW("read-only mount %s on %s failed (%s) !!!\n", device_name, vol->mount_point, strerror(errno));
			}
		}
	}

	return -1;
auto_mounted:
	return 0;
}

//Rony modify for sdcard cannot mounted
int check_device_mounted(Volume *vol, char *name){
    int i, len;
    char * tmp;
    char device_name[256];
    char *mounted_device;
    if (vol->blk_device != NULL) {
        tmp = strchr(name, '#');
        len = tmp - name;
        if (tmp && len < 255) {
            strncpy(device_name, name, len);

            for (i = 0; i <= NUM_OF_PARTITION_TO_ENUM; i++) {
                device_name[len] = '0' + i; 
                device_name[len + 1] = '\0';
                LOGW("try mount1 %s ...\n", device_name);

				if (!auto_mount_fs(device_name, vol)) {
					mounted_device = device_name;
					goto mounted;
				}
            }

            device_name[len] = '\0';
            LOGW("try mount2 %s ...\n", device_name);

            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        } else {
            LOGW("try mount3 %s ...\n", vol->blk_device);
            strncpy(device_name, vol->blk_device, sizeof(device_name));
            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        }
    }
	
    //LOGE("failed to mount %s (%s)\n", vol->mount_point, strerror(errno));
    return -1;
mounted:	
	return 0;
}

int smart_device_mounted(Volume *vol) {
    int i, len;
    char * tmp;
    char device_name[256];
    char *mounted_device;

    mkdir(vol->mount_point, 0755); 

#if 0 //Rony modify for sdcard cannot mounted
    if (vol->blk_device != NULL) {
        tmp = strchr(vol->blk_device, '#');
        len = tmp - vol->blk_device;
        if (tmp && len < 255) {
            strncpy(device_name, vol->blk_device, len);

            for (i = 1; i <= NUM_OF_PARTITION_TO_ENUM; i++) {
                device_name[len] = 48 + i;
                device_name[len + 1] = '\0';
                LOGW("try mount %s ...\n", device_name);

				if (!auto_mount_fs(device_name, vol)) {
					mounted_device = device_name;
					goto mounted;
				}
            }

            device_name[len] = '\0';
            LOGW("try mount %s ...\n", device_name);

            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        } else {
            LOGW("try mount %s ...\n", vol->blk_device);
            strncpy(device_name, vol->blk_device, sizeof(device_name));
            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        }
    }
#else //Rony modify for sdcard cannot mounted
    if (vol->blk_device != NULL) {
		int str_len = 0;
		strcpy(device_name, vol->blk_device);
        tmp = strchr(device_name, '#');	
		len = tmp - device_name;
		str_len = strlen(device_name) - 1; 
		if(len < str_len && len != str_len){
			//LOGW("try mount %s ...\n", vol->blk_device);
			for(i = 0; i < NUM_OF_PARTITION_TO_ENUM; i++){
				*tmp = '0' + i;
				if(!check_device_mounted(vol, device_name)){
					goto mounted;
				}
			}
		}else{
			if(!check_device_mounted(vol, vol->blk_device)){
				goto mounted;
			}
		}
    }
	
#endif

    if (vol->device2 != NULL) {
        tmp = strchr(vol->device2, '#');
        len = tmp - vol->device2;
        if (tmp && len < 255) {
            strncpy(device_name, vol->device2, len);

            for (i = 0; i <= NUM_OF_PARTITION_TO_ENUM; i++) {
                device_name[len] = '0' + i;
                device_name[len + 1] = '\0';
                LOGW("try mount21 %s ...\n", device_name);

                if (!auto_mount_fs(device_name, vol)) {
                    mounted_device = device_name;
                    goto mounted;
                }
            }

            device_name[len] = '\0';
            LOGW("try mount22 %s ...\n", device_name);

            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        } else {
            LOGW("try mount23 %s ...\n", vol->device2);
            strncpy(device_name, vol->device2, sizeof(device_name));
            if (!auto_mount_fs(device_name, vol)) {
                mounted_device = device_name;
                goto mounted;
            }
        }
    }

    LOGE("failed to mount %s (%s)\n", vol->mount_point, strerror(errno));
    return -1;

mounted:
    //ui_print("Device %s mounted\n", mounted_device);
    return 0;
}
int ensure_path_mounted(const char* path) {
    Volume* v = volume_for_path(path);
    if (v == NULL) {
        LOGE("unknown volume for path [%s]\n", path);
        return -1;
    }
    if (strcmp(v->fs_type, "ramdisk") == 0) {
        // the ramdisk is always mounted.
        return 0;
    }

    int result;
    result = scan_mounted_volumes();
    if (result < 0) {
        LOGE("failed to scan mounted volumes\n");
        return -1;
    }

    const MountedVolume* mv =
        find_mounted_volume_by_mount_point(v->mount_point);
    if (mv) {
        // volume is already mounted
        return 0;
    }

    mkdir(v->mount_point, 0755);  // in case it doesn't already exist

    if (strcmp(v->fs_type, "yaffs2") == 0) {
        // mount an MTD partition as a YAFFS2 filesystem.
        mtd_scan_partitions();
        const MtdPartition* partition;
        partition = mtd_find_partition_by_name(v->blk_device);
        if (partition == NULL) {
            LOGE("failed to find \"%s\" partition to mount at \"%s\"\n",
                 v->blk_device, v->mount_point);
            return -1;
        }
        return mtd_mount_partition(partition, v->mount_point, v->fs_type, 0);
    } else if (strcmp(v->fs_type, "ext4") == 0 ||
               strcmp(v->fs_type, "vfat") == 0 ||
			   strcmp(v->fs_type, "auto") == 0) {
        if ((strcmp(v->fs_type, "vfat") == 0 || strstr(v->fs_type, "auto")) &&
            (strstr(v->mount_point, "sdcard") || strstr(v->mount_point, "udisk"))) {
            int time_out = 100000;
            while (time_out) {
                if (!smart_device_mounted(v)) {
                    return 0;
                }
                usleep(100000);
                time_out -= 100000;
            }
            LOGE("cannot mount %s (%s)\n", v->mount_point, strerror(errno));
            return -1;
        } else {
            result = mount(v->blk_device, v->mount_point, v->fs_type,
                       MS_NOATIME | MS_NODEV | MS_NODIRATIME, "");
            if (result == 0) return 0;

            LOGE("failed to mount %s (%s)\n", v->mount_point, strerror(errno));
            return -1;
        }
#if UBI_SUPPORT
    } else if (strcmp(v->fs_type, "ubifs") == 0) {
        LOGI("ensure_path_mounted ubifs:  %s %s %s %s\n", v->mount_point, v->fs_type,
               v->blk_device, v->blk_device);
        libubi_t libubi;
        struct ubi_info ubi_info;
        struct ubi_dev_info dev_info;
        struct ubi_attach_request req;
        int err;
        char value[32] = {0};

        mtd_scan_partitions();
        int mtdn = mtd_get_index_by_name(v->blk_device);
        if (mtdn < 0) {
            LOGE("bad mtd index for %s\n", v->blk_device);
            return -1;
        }

        libubi = libubi_open();
        if (!libubi) {
            LOGE("libubi_open fail\n");
            return -1;
        }

        /*
         * Make sure the kernel is fresh enough and this feature is supported.
         */
        err = ubi_get_info(libubi, &ubi_info);
        if (err) {
            LOGE("cannot get UBI information\n");
            goto out_ubi_close;
        }

        if (ubi_info.ctrl_major == -1) {
            LOGE("MTD attach/detach feature is not supported by your kernel\n");
            goto out_ubi_close;
        }

        req.dev_num = UBI_DEV_NUM_AUTO;
        req.mtd_num = mtdn;
        req.vid_hdr_offset = 0;
        req.mtd_dev_node = NULL;

        // make sure partition is detached before attaching
        ubi_detach_mtd(libubi, DEFAULT_CTRL_DEV, mtdn);

        err = ubi_attach(libubi, DEFAULT_CTRL_DEV, &req);
        if (err) {
            LOGE("cannot attach mtd%d", mtdn);
            goto out_ubi_close;
        }

        /* Print some information about the new UBI device */
        err = ubi_get_dev_info1(libubi, req.dev_num, &dev_info);
        if (err) {
            LOGE("cannot get information about newly created UBI device\n");
            goto out_ubi_detach;
        }

        sprintf(value, "/dev/ubi%d_0", dev_info.dev_num);

        /* Print information about the created device */
        //err = ubi_get_vol_info1(libubi, dev_info.dev_num, 0, &vol_info);
        //if (err) {
        //  LOGE("cannot get information about UBI volume 0");
        //  goto out_ubi_detach;
        //}

        if (mount(value, v->mount_point, v->fs_type,  MS_NOATIME | MS_NODEV | MS_NODIRATIME, NULL )) {
            LOGE("cannot mount ubifs %s to %s\n", value, v->mount_point);
            goto out_ubi_detach;
        }
        LOGI("mount ubifs successful  %s to %s\n", value, v->mount_point);

        libubi_close(libubi);
        return 0;

out_ubi_detach:
        ubi_detach_mtd(libubi, DEFAULT_CTRL_DEV, mtdn);

out_ubi_close:
        libubi_close(libubi);
        return -1;
#endif //UBI_SUPPORT
    }
    LOGE("unknown fs_type \"%s\" for %s\n", v->fs_type, v->mount_point);
    return -1;
}

int ensure_path_unmounted(const char* path) {
    int ret;
    Volume* v = volume_for_path(path);
    if (v == NULL) {
        LOGE("unknown volume for path [%s]\n", path);
        return -1;
    }
    if (strcmp(v->fs_type, "ramdisk") == 0) {
        // the ramdisk is always mounted; you can't unmount it.
        return -1;
    }

    int result;
    result = scan_mounted_volumes();
    if (result < 0) {
        LOGE("failed to scan mounted volumes\n");
        return -1;
    }

    const MountedVolume* mv =
        find_mounted_volume_by_mount_point(v->mount_point);
    if (mv == NULL) {
        // volume is already unmounted
        return 0;
    }

    if (strcmp(v->fs_type, "ubifs") != 0) {
        return unmount_mounted_volume(mv);
    } else {
#if UBI_SUPPORT
        libubi_t libubi;
        struct ubi_info ubi_info;

        unmount_mounted_volume(mv);

        mtd_scan_partitions();
        int mtdn = mtd_get_index_by_name(v->blk_device);
        if (mtdn < 0) {
            LOGE("bad mtd index for %s\n", v->blk_device);
            return -1;
        }

        libubi = libubi_open();
        if (!libubi) {
            LOGE("libubi_open fail\n");
            return -1;
        }

        /*
         * Make sure the kernel is fresh enough and this feature is supported.
         */
        ret = ubi_get_info(libubi, &ubi_info);
        if (ret) {
            LOGE("cannot get UBI information\n");
            goto out_ubi_close;
        }

        if (ubi_info.ctrl_major == -1) {
            LOGE("MTD detach/detach feature is not supported by your kernel\n");
            goto out_ubi_close;
        }

        ret = ubi_detach_mtd(libubi, DEFAULT_CTRL_DEV, mtdn);
        if (ret) {
            LOGE("cannot detach mtd%d\n", mtdn);
            goto out_ubi_close;
        }
        LOGI("detach ubifs successful mtd%d\n", mtdn);

        libubi_close(libubi);
        return 0;

    out_ubi_close:
        libubi_close(libubi);
#endif //UBI_SUPPORT
        return -1;
    }
}

int format_volume(const char* volume) {
    Volume* v = volume_for_path(volume);
    if (v == NULL) {
        LOGE("unknown volume \"%s\"\n", volume);
        return -1;
    }
    if (strcmp(v->fs_type, "ramdisk") == 0) {
        // you can't format the ramdisk.
        LOGE("can't format_volume \"%s\"", volume);
        return -1;
    }
    if (strcmp(v->mount_point, volume) != 0) {
        LOGE("can't give path \"%s\" to format_volume\n", volume);
        return -1;
    }

    if (ensure_path_unmounted(volume) != 0) {
        LOGE("format_volume failed to unmount \"%s\"\n", v->mount_point);
        return -1;
    }

    if (strcmp(v->fs_type, "yaffs2") == 0 || strcmp(v->fs_type, "mtd") == 0 ||
        strcmp(v->fs_type, "ubifs") == 0) {
        mtd_scan_partitions();
        const MtdPartition* partition = mtd_find_partition_by_name(v->blk_device);
        if (partition == NULL) {
            LOGE("format_volume: no MTD partition \"%s\"\n", v->blk_device);
            return -1;
        }

        MtdWriteContext *write = mtd_write_partition(partition);
        if (write == NULL) {
            LOGW("format_volume: can't open MTD \"%s\"\n", v->blk_device);
            return -1;
        } else if (mtd_erase_blocks(write, -1) == (off_t) -1) {
            LOGW("format_volume: can't erase MTD \"%s\"\n", v->blk_device);
            mtd_write_close(write);
            return -1;
        } else if (mtd_write_close(write)) {
            LOGW("format_volume: can't close MTD \"%s\"\n", v->blk_device);
            return -1;
        }
        if (strcmp(v->fs_type, "ubifs") == 0) {
#if UBI_SUPPORT
            return format_ubifs_volume(v->blk_device);
#else
            return -1;
#endif
        }
        return 0;
    }

    if (strcmp(v->fs_type, "ext4") == 0) {
	int tmp = ext4_erase_volum(volume);
	if(tmp){
		LOGE("format_volume: ext4_erase_volum failed on %s\n", v->blk_device);
		return -1;
	}
        int result = make_ext4fs(v->blk_device, v->length, volume, sehandle);
        if (result != 0) {
            LOGE("format_volume: make_extf4fs failed on %s\n", v->blk_device);
            return -1;
        }
        return 0;
    }

    if (strcmp(v->fs_type, "vfat") == 0) {
        char value[PROPERTY_VALUE_MAX+1];
        property_get("ro.media.partition.label", value, RECOVERY_MEDIA_LABEL);
        char *argv[] = { "/sbin/busybox",
                         "mkfs.vfat",
                         "-n",
                         value,
                         (char*)v->blk_device,
                         NULL };
/*        
        char *fdiskargv[] = { "/sbin/fdisk.media.sh",
                              NULL };
 */
        int result = 0;
        pid_t pid = 0;
/*
        pid = fork();
        if (pid == 0) {
            result = execv("/sbin/fdisk.media.sh", NULL);
            if (result)
                LOGE("format_volume: fdisk.media failed on %s (%s)\n", v->blk_device, strerror(errno));
            _exit(-1);
        }
        waitpid(pid, &result, 0);
        if (WIFEXITED(result)) {
            if (WEXITSTATUS(result) != 0) {
                LOGE("format_volume: fdisk.media failed on %s (%d)\n", v->blk_device, WEXITSTATUS(result));
                result = -1;
            }
            else
                result = 0;
        }
        else if (WIFSIGNALED(result)) {
            LOGE("format_volume: fdisk.media terminated on %s (%d)\n", v->blk_device, WTERMSIG(result));
            result = -1;
        }
        else
            result = 0;
 */
        result = CmdRuner::run_command("/sbin/fdisk.media.sh");
        if(!result)
            LOGE("run_command(\"/sbin/fdisk.media.sh\") successful\n");
        else
            LOGE("run_command(\"/sbin/fdisk.media.sh\") failed\n");
        
        {
            struct stat st;
            if (stat(v->blk_device, &st) != 0) {
                if (v->device2)
                    argv[4] = (char *)v->device2;
            }
        }

/*
        result = 0;
        pid = fork();
        if (pid == 0) {
            result = execv("/sbin/busybox", argv);
            if (result)
                LOGE("format_volume: Failed to format %s (%s)\n", v->blk_device, strerror(errno));
            _exit(-1);
        }
        waitpid(pid, &result, 0);
        if (WIFEXITED(result)) {
           if (WEXITSTATUS(result) != 0) {
               LOGE("format_volume: mkfs.vfat failed on %s (%d)\n", v->device, WEXITSTATUS(result));
               result = -1;
           }
           else
               result = 0;
        }
        else if (WIFSIGNALED(result)) {
            LOGE("format_volume: mkfs.vfat terminated on %s (%d)\n", v->device, WTERMSIG(result));
            result = -1;
        }
        else
           result = 0;
 */
        result = CmdRuner::run_command("/sbin/busybox", argv);
        if(!result)
            LOGE("run_command(\"/sbin/busybox\") successful\n");
        else
            LOGE("run_command(\"/sbin/busybox\") failed\n");
       
        return result;
    }

    LOGE("format_volume: fs_type \"%s\" unsupported\n", v->fs_type);
    return -1;
}

#if UBI_SUPPORT
static int format_ubifs_volume(const char* location) {
    int err;
    struct ubi_info ubi_info;
    struct ubi_dev_info dev_info;
    struct ubi_attach_request req;
    struct ubi_mkvol_request req2;
    char ubinode[16] ={0};

    mtd_scan_partitions();
    int mtdn = mtd_get_index_by_name(location);
    if (mtdn < 0) {
        LOGE("bad mtd index for %s\n", location);
        return -1;
    }

    libubi_t libubi;
    libubi = libubi_open();
    if (!libubi) {
        LOGE("libubi_open fail\n");
        return -1;
    }

    /*
     * Make sure the kernel is fresh enough and this feature is supported.
     */
    err = ubi_get_info(libubi, &ubi_info);
    if (err) {
        LOGE("cannot get UBI information\n");
        goto out_ubi_close;
    }

    if (ubi_info.ctrl_major == -1) {
        LOGE("MTD attach/detach feature is not supported by your kernel\n");
        goto out_ubi_close;
    }

    req.dev_num = UBI_DEV_NUM_AUTO;
    req.mtd_num = mtdn;
    req.vid_hdr_offset = 0;
    req.mtd_dev_node = NULL;

    err = ubi_attach(libubi, DEFAULT_CTRL_DEV, &req);
    if (err) {
        LOGE("cannot attach mtd%d", mtdn);
        goto out_ubi_close;
    }
    usleep(100000);
    /* Print some information about the new UBI device */
    err = ubi_get_dev_info1(libubi, req.dev_num, &dev_info);
    if (err) {
        LOGE("cannot get information about newly created UBI device\n");
        goto out_ubi_detach;
    }

    req2.vol_id = UBI_VOL_NUM_AUTO;
    req2.alignment = 1;
    req2.bytes = dev_info.avail_bytes;// dev_info.avail_lebs*dev_info.leb_size;
    req2.name = location;
    req2.vol_type = UBI_DYNAMIC_VOLUME;

    sprintf(ubinode, "/dev/ubi%d", dev_info.dev_num);

    err = ubi_mkvol(libubi, ubinode, &req2);
    if (err < 0) {
        LOGE("cannot UBI create volume %s at %s %d %llu\n", req2.name, ubinode ,err, req2.bytes);
        goto out_ubi_detach;
    }

    ubi_detach_mtd(libubi, DEFAULT_CTRL_DEV, mtdn);
    libubi_close(libubi);
    return 0;

out_ubi_detach:
    ubi_detach_mtd(libubi, DEFAULT_CTRL_DEV, mtdn);

out_ubi_close:
    libubi_close(libubi);
    return -1;
}
#endif //UBI_SUPPORT

int setup_install_mounts() {
    if (fstab == NULL) {
        LOGE("can't set up install mounts: no fstab loaded\n");
        return -1;
    }
    for (int i = 0; i < fstab->num_entries; ++i) {
        Volume* v = fstab->recs + i;

        if (strcmp(v->mount_point, "/tmp") == 0 ) {
            if (ensure_path_mounted(v->mount_point) != 0) return -1;
        } else if(strcmp(v->mount_point, "/cache") == 0){
            if (ensure_path_mounted(v->mount_point) != 0){
		format_volume("/cache");
	        if (ensure_path_mounted(v->mount_point) != 0) return -1;
	    }
        }else {
            if (ensure_path_unmounted(v->mount_point) != 0) return -1;
        }
    }
    return 0;
}
