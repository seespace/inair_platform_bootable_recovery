#ifndef __EFUSE_H_
#define __EFUSE_H_

#include <linux/ioctl.h>
#include "device.h"

#define EFUSE_ENCRYPT_DISABLE   _IO('f', 0x10)
#define EFUSE_ENCRYPT_ENABLE    _IO('f', 0x20)
#define EFUSE_ENCRYPT_RESET     _IO('f', 0x30)
#define EFUSE_INFO_GET			_IO('f', 0x40)

#define MAX_EFUSE_BYTES         	512

#define EFUSE_NONE_ID			0
#define EFUSE_VERSION_ID		1
#define EFUSE_LICENCE_ID		2
#define EFUSE_MAC_ID				3
#define EFUSE_MAC_WIFI_ID	4
#define EFUSE_MAC_BT_ID		5
#define EFUSE_HDCP_ID			6
#define EFUSE_USID_ID				7
#if defined(MESON6) || defined(MESON8) 
#define EFUSE_MACHINEID_ID			10  
#endif


#if defined(MESON6) || defined(MESON8) 
//M6 platform
typedef struct efuseinfo_item{
	char title[40];	
	unsigned id;
	// unsigned offset;    // write offset	
	long long offset;  // write offset, match common/include/linux/efuse.h
	unsigned enc_len;
	unsigned data_len;	
	int bch_en;
	int bch_reverse;
} efuseinfo_item_t;

typedef enum {
	EFUSE_NONE = 0,
	EFUSE_LICENCE,
	EFUSE_MAC,
	EFUSE_HDCP,
	EFUSE_MAC_BT,
	EFUSE_MAC_WIFI,
	EFUSE_USID,
	EFUSE_VERSION,
	EFUSE_MACHINEID,	  				
	EFUSE_TYPE_MAX,
} efuse_type_t;

static char* efuse_title[EFUSE_TYPE_MAX] = {
	NULL,
	"licence",
	"mac",
	"hdcp",
	"mac_bt",
	"mac_wifi",
	"usid",
	"version",
	"machineid",						
};

static unsigned int efuse_id[EFUSE_TYPE_MAX] = {
    EFUSE_NONE_ID,
    EFUSE_LICENCE_ID,
    EFUSE_MAC_ID,
    EFUSE_HDCP_ID,
    EFUSE_MAC_BT_ID,
    EFUSE_MAC_WIFI_ID,
    EFUSE_USID_ID,
    EFUSE_VERSION_ID,
    EFUSE_MACHINEID_ID,					
};

#elif defined(MESON3) 
//M3 platform
typedef struct efuseinfo_item{
	char title[40];	
	unsigned id;
	unsigned offset;    // write offset	
	unsigned data_len;		
} efuseinfo_item_t;

typedef enum {
	EFUSE_NONE = 0,
	EFUSE_LICENCE,
	EFUSE_MAC,
	EFUSE_HDCP,
	EFUSE_MAC_BT,
	EFUSE_MAC_WIFI,
	EFUSE_USID,
	EFUSE_VERSION,
	EFUSE_TYPE_MAX,
} efuse_type_t;

static char* efuse_title[EFUSE_TYPE_MAX] = {
	NULL,
	"licence",
	"mac",
	"hdcp",
	"mac_bt",
	"mac_wifi",
	"usid",
	"version",
};

static unsigned int efuse_id[EFUSE_TYPE_MAX] = {
    EFUSE_NONE_ID,
    EFUSE_LICENCE_ID,
    EFUSE_MAC_ID,
    EFUSE_HDCP_ID,
    EFUSE_MAC_BT_ID,
    EFUSE_MAC_WIFI_ID,
    EFUSE_USID_ID,
    EFUSE_VERSION_ID,
};
#endif

//add tzh for hdcp begin
#ifdef EFUSE_HDCP_ENABLE
#define DWORD unsigned int  //4 bytes
#define BYTE unsigned char   //1 byte
#define SHA1_MAC_LEN 20

typedef struct {
	DWORD state[5];
	DWORD count[2];
	BYTE buffer[64];
} SHA1_CTX;

typedef struct
{
	unsigned char ksv[5];
	unsigned char rsv[3];
	unsigned char dpk[280];
	unsigned char sha[20];
}hdcp_llc_file;
#define EFUSE_PATH_VERSION                                "/sys/class/aml_keys/aml_keys/version"
#define EFUSE_PATH_KEY_READ                               "/sys/class/aml_keys/aml_keys/key_read"
#define EFUSE_PATH_KEY_WRITE                              "/sys/class/aml_keys/aml_keys/key_write"
#endif    

#define EFUSE_DEVICE_NAME	"/dev/efuse"

int recovery_efuse(int confirm, const char* args, Device * device);

#endif

