#ifndef _USB_BURNING_H
#define _USB_BURNING_H

extern RecoveryUI* ui;
int erase_volume(const char *volume);
int usb_burning_main(int opt);
void copy_log_file(const char* source, const char* destination, int append);

/*
 *  write to efuse/nand when usb_burning 
 *  WRITE_TO_EFUSE_ENABLE and WRITE_TO_NAND_ENABLE should not be both existed
 */
//#define WRITE_TO_EFUSE_ENABLE	
#define WRITE_TO_NAND_ENABLE

#if defined(WRITE_TO_EFUSE_ENABLE) && defined(WRITE_TO_NAND_ENABLE)
#error You should only select one of WRITE_TO_EFUSE_ENABLE and WRITE_TO_NAND_ENABLE
#endif

#if defined(WRITE_TO_EFUSE_ENABLE)
int efuse_read_version(char* version_str,  RecoveryUI* ui);
int efuse_write_version(char* version_str,  RecoveryUI* ui);
int efuse_read_mac(char *mac_str,int type, RecoveryUI* ui);
int efuse_write_mac(char *mac_str,int type, RecoveryUI* ui);
int efuse_read_mac_bt(char *mac_bt_str,int type, RecoveryUI* ui);
int efuse_write_mac_bt(char *mac_bt_str,int type, RecoveryUI* ui);
int efuse_read_mac_wifi(char *mac_wifi_str,int type, RecoveryUI* ui);
int efuse_write_mac_wifi(char *mac_wifi_str,int type, RecoveryUI* ui);
int efuse_read_usid(char *usid_str,int type, RecoveryUI* ui);
int efuse_write_usid(char *usid_str,int type, RecoveryUI* ui);
int efuse_read_hdcp(char *hdcp_str,int type, RecoveryUI* ui);
int efuse_write_hdcp(char *hdcp_str,int type, RecoveryUI* ui);
#elif defined(WRITE_TO_NAND_ENABLE)
int nand_write_version(char *path, char *version_str);
int nand_read_mac(char *path, char *mac_str);
int nand_write_mac(char *path, char *mac_str);
int nand_read_mac_bt(char *path, char *mac_bt_str);
int nand_write_mac_bt(char *path, char *mac_bt_str);
int nand_read_mac_wifi(char *path, char *mac_wifi_str);
int nand_write_mac_wifi(char *path, char *mac_wifi_str);
int nand_read_usid(char *path, char *usid_str);
int nand_write_usid(char *path, char *usid_str);
int nand_read_hdcp(char *path, char *hdcp_str);
int nand_write_hdcp(char *path, char *hdcp_str);
#endif
#endif

