#ifndef  _OPERATE_EEPROM_H_ 
#define _OPERATE_EEPROM_H_
typedef struct t_hwver_mac_addr{
    int magic;
    char mac[6];
}hwver_mac_addr;


typedef struct t_hwver_use_id{
    int magic;
    char use_id[44];
}hwver_use_id;

struct t_key_info{
    int index;
    char *key;
	struct t_key_info *next;
};


/* hwversion functions */
typedef struct t_HwVersion {
	int magic           :32;        //0

	short version       :16;        //4
	short length        :16;        //6

	int crc             :32;        //8
	int dtbversion      :32;        //12    

	char productinfo    :8;     //16
	char boardinfo      :8;     //17
	char ddrtype        :4;     //18
	char ddrname        :4;
	char ddrsize        :4;     //19
	char flashtype      :4;

	char flashname      :4;     //20
	char flashsize      :4;
	char spienable      :1;     //21
	char spisize        :3;
	char spiname        :4;
	char eepromenable   :1;     //22
	char eepromsize     :3;
	char eepromname     :4;
	char ethenable      :1;     //23
	char ethinfo        :3;
	char wifienable     :1;
	char wifitype       :3;

	char wifiname       :8;     //24
	char lcdenable      :1;     //25
	char lcdtype        :3;
	char lcdsize        :4;
	char touchenable    :1;     //26
	char touchtype      :3;
	char touchsize      :4;
	char pmuenable      :1;     //27
	char pmuname        :3;
	char serialenable   :1;
	char serialname     :3;

	char vgaenable      :1;     //28
	char cvbsenable     :1;
	char ypbprenable    :1;
	char hdmienable     :1;
	char hdmicec        :1;
	char hdmihdcp       :1;
	char btenable       :1;
	char recovery       :1;
	char hdmiinen       :1;     //29
	char hdmiintype     :3;
	char hdmiinname     :4;

	char dtvenable      :1;     //30
	char dtvtype        :3;
	char dtvname        :4;

	char oemid          :8;     //31

}m_HwVersion;

typedef struct t_IoctlHwVersion{
	int is_wr;
	m_HwVersion hwver;
}RwHwVersion;

enum{
	KEY_TYPE_MAC = 0,
	KEY_TYPE_USID,
	KEY_TYPE_HDCP,
	KEY_TYPE_HDCP2
};


#define HWVER_DEV  "/dev/hw_version"
#define SYS_MAC_PATH "/sys/class/hw_version/hw_version/mac" 
#define SYS_USE_ID_PATH "/sys/class/hw_version/hw_version/use_id" 
#define  MAC_SIZE       (sizeof(hwver_mac_addr))
#define  USE_ID_SIZE    (sizeof(hwver_use_id))
#define  HARDWARE_SIZE    (sizeof(m_HwVersion))
#define  HARDWARE_OFFSET   0
#define  MAC_OFFSET       40
#define  USE_ID_OFFSET    50
#define   MAGIC   0x47656e69


#define  HWVER_HARDWARE_VERSION   _IOWR('C',  0,RwHwVersion)
#define  HWVER_MAC   _IOW('C',  1, hwver_mac_addr)
#define  HWVER_USE_ID   _IOW('C',  2, hwver_use_id)
#define  HWVER_WP_ENABLE   _IOW('C',  3, int)
#define  HWVER_MAC_READ   _IOR('C',  4, hwver_mac_addr)
#define  HWVER_USE_ID_READ   _IOR('C',  5, hwver_use_id)

#define EEPROM_MENU_MAX  4  
#define LINE_BUF_SIZE 120

#define KEY_MASK			0x0F
#define KEY_ETHMAC_FLAG	0x01
#define KEY_USID_FLAG		0x02
#define KEY_HDCP_FLAG		0x04
#define KEY_HDCP2_FLAG		0x08


#define   SHOW_HWVER(data,strinfo){\
	printf("%s",strinfo);\
	printf("version=%d\n", data.version);\
	printf("length=%d\n", data.length);\
	printf("crc=0x%02x\n", data.crc);\
	printf("dtbversion=0x%02x\n", data.dtbversion);\
	\
	printf("productinfo=0x%02x\n", data.productinfo);\
	printf("flashsize=0x%02x\n", data.flashsize);\
	printf("ddrtype=0x%02x\n", data.ddrtype);\
	printf("flashtype=0x%02x\n", data.flashtype);\
	printf("ddrname=0x%02x\n", data.ddrname);\
	printf("ddrsize=0x%02x\n", data.ddrsize);\
	\
	printf("flashname=%02x\n", data.flashname);\
	printf("flashsize=0x%02x\n", data.flashsize);\
	printf("spienable=0x%02x\n", data.spienable);\
	printf("spisize=%02x\n", data.spisize);\
	printf("spiname=%02x", data.spiname);\
	printf("eepromenable=%02x\n", data.eepromenable);\
	printf("eepromsize=%02x\n", data.eepromsize);\
	printf("eepromname=%02x\n", data.eepromname);\
	printf("ethenable=%02x\n", data.ethenable);\
	printf("ethinfo=0x%02x\n", data.ethinfo);\
	printf("wifienable=%02x\n", data.wifienable);\
	printf("wifitype=0x%02x\n", data.wifitype);\
	\
	printf("wifiname=0x%02x\n", data.wifiname);\
	printf("lcdenable=0x%02x\n", data.lcdenable);\
	printf("lcdtype=0x%02x\n", data.lcdtype);\
	printf("lcdsize=0x%02x\n", data.lcdsize);\
	printf("touchenable=0x%02x\n", data.touchenable);\
	printf("touchtype=0x%02x\n", data.touchtype);\
	printf("touchsize=0x%02x\n", data.touchsize);\
	printf("pmuenable=0x%02x\n", data.pmuenable);\
	printf("pmuname=0x%02x\n", data.pmuname);\
	printf("serialenable=0x%02x\n", data.serialenable);\
	printf("serialname=0x%02x\n", data.serialname);\
	\
	printf("vgaenable=0x%02x\n", data.vgaenable);\
	printf("cvbsenable=0x%02x\n", data.cvbsenable);\
	printf("ypbprenable=0x%02x\n", data.ypbprenable);\
	printf("hdmienable=0x%02x\n", data.hdmienable);\
	printf("hdmicec=0x%02x\n", data.hdmicec);\
	printf("hdmihdcp=0x%02x\n", data.hdmihdcp);\
	printf("btenable=0x%02x\n", data.btenable);\
	printf("recovery=0x%02x\n", data.recovery);\
	printf("hdmiinen=0x%02x\n", data.hdmiinen);\
	printf("hdmiintype=0x%02x\n", data.hdmiintype);\
	printf("hdmiinname=0x%02x\n", data.hdmiinname);\
	\
	printf("dtvenable=0x%02x\n", data.dtvenable);\
	printf("dtvtype=0x%02x\n", data.dtvtype);\
	printf("dtvname=0x%02x\n", data.dtvname);\
	printf("oemid=0x%02x\n", data.oemid);}

typedef enum {
  EEPROM_NONE = 0,
  BURN_KEY,
  CLEAN_KEY,
  BURN_HARDVERSION,
  GET_HARDVERSION,
  EEPROM_TYPE_MAX
}item_id;

typedef enum t_WifiName{                                               
	WIFI_NAME_USB,                                                     
	WIFI_NAME_AP6330,                                                  
	WIFI_NAME_AP6181,                                                  
	WIFI_NAME_AP6210,                                                  
	WIFI_NAME_BCM40181,                                                
	WIFI_NAME_BCM40183,                                                
	WIFI_NAME_MAX                                                      
}m_WifiName;

typedef enum t_DtbType{                                                
	DTB_NORMAL_VER1 = 0,                                               
	DTB_NORMAL_VER2,                                                   
	DTB_NORMAL_VER3,                                                   
	DTB_NORMAL_VER4,                                                   
	DTB_NORMAL_VER5,                                                   
	DTB_NORMAL_VER6,                                                   
	DTB_NORMAL_VER7,                                                   
	DTB_NORMAL_VER8,                                                   
	DTB_DOGNLE_VER1 = 0x100,                                           
	DTB_DOGNLE_VER2,
	DTB_DOGNLE_VER3,
	DTB_DOGNLE_VER4,
	DTB_DOGNLE_VER5,
	DTB_DOGNLE_VER6,
	DTB_DOGNLE_VER7,
	DTB_DOGNLE_VER8,
	DTB_HDMIIN_VER1 = 0x10000,
	DTB_HDMIIN_VER2,
	DTB_HDMIIN_VER3,
	DTB_HDMIIN_VER4,
	DTB_HDMIIN_VER5,
	DTB_HDMIIN_VER6,
	DTB_HDMIIN_VER7,
	DTB_HDMIIN_VER8,
}m_DtbType;


static char* eeprom_title[EEPROM_TYPE_MAX] = {
	NULL,
	"Burn keys",
	"Clean keys",
	"Burn hardversion",
    "Get hardversion",
};

int recovery_operate_eeprom(int interactive, const char* args, Device * device);

#endif


