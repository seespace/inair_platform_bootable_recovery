#ifndef __CMD_EXCUTE_H_
#define __CMD_EXCUTE_H_

#define MAX_ARGS_NUM	16
int recovery_run_cmd(char* command_args);

class CmdRuner{
  public:
static int run_command(char* command);
static int run_command(char* command, char **parameters);
};
#endif