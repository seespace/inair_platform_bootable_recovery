LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := graphics.c events.c resources.c

LOCAL_C_INCLUDES +=\
    external/libpng\
    external/zlib

LOCAL_MODULE := libminui

# This used to compare against values in double-quotes (which are just
# ordinary characters in this context).  Strip double-quotes from the
# value so that either will work.

ifeq ($(subst ",,$(TARGET_RECOVERY_PIXEL_FORMAT)),RGBX_8888)
  LOCAL_CFLAGS += -DRECOVERY_RGBX
endif
ifeq ($(subst ",,$(TARGET_RECOVERY_PIXEL_FORMAT)),BGRA_8888)
  LOCAL_CFLAGS += -DRECOVERY_BGRA
endif

ifeq ($(TARGET_RECOVERY_ROTATE), 0)
LOCAL_CFLAGS += -DRECOVERY_ROTATE_0
endif
ifeq ($(TARGET_RECOVERY_ROTATE), 90)
LOCAL_CFLAGS += -DRECOVERY_ROTATE_90
endif
ifeq ($(TARGET_RECOVERY_ROTATE), 180)
LOCAL_CFLAGS += -DRECOVERY_ROTATE_180
endif
ifeq ($(TARGET_RECOVERY_ROTATE), 270)
LOCAL_CFLAGS += -DRECOVERY_ROTATE_270
endif

ifeq ($(TARGET_RECOVERY_FONT_10X18), true)
LOCAL_CFLAGS += -DRECOVERY_FONT_10X18
endif
ifeq ($(TARGET_RECOVERY_FONT_7X16), true)
LOCAL_CFLAGS += -DRECOVERY_FONT_7X16
endif
ifeq ($(TARGET_RECOVERY_ROBOTO_10X18), true)
LOCAL_CFLAGS += -DRECOVERY_ROBOTO_10X18
endif
ifeq ($(TARGET_RECOVERY_ROBOTO_15X24), true)
LOCAL_CFLAGS += -DRECOVERY_ROBOTO_15X24
endif

ifneq ($(TARGET_RECOVERY_OVERSCAN_PERCENT),)
  LOCAL_CFLAGS += -DOVERSCAN_PERCENT=$(TARGET_RECOVERY_OVERSCAN_PERCENT)
else
  LOCAL_CFLAGS += -DOVERSCAN_PERCENT=0
endif

include $(BUILD_STATIC_LIBRARY)
